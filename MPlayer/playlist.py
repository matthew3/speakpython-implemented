#Boa:Dialog:create_playlist

##///////////////////////////////////////////////////////////////////////////
## Name:        playlist.py
## Purpose:     Playlist creation dialog of MPlayer Control
## Author:      Tamas Zelena
## Modified by:
## Created:     09/07/2004
## Copyright:   (c) Tamas Zelena
## Licence:     GNU-GPL
##///////////////////////////////////////////////////////////////////////////

import wx
import os

def create(parent):
    return create_playlist(parent)

[wxID_CREATE_PLAYLIST, wxID_CREATE_PLAYLISTBITMAPBUTTON1,
 wxID_CREATE_PLAYLISTBITMAPBUTTON2, wxID_CREATE_PLAYLISTBITMAPBUTTON3,
 wxID_CREATE_PLAYLISTBITMAPBUTTON4, wxID_CREATE_PLAYLISTBUTTON2,
 wxID_CREATE_PLAYLISTLISTVIEW1, wxID_CREATE_PLAYLISTMOVEDOWN,
 wxID_CREATE_PLAYLISTMOVEUP, wxID_CREATE_PLAYLISTPLNAME,
 wxID_CREATE_PLAYLISTSTATICLINE1, wxID_CREATE_PLAYLISTSTATICTEXT1,
] = [wx.NewId() for _init_ctrls in range(12)]

class create_playlist(wx.Dialog):
    def _init_coll_listView1_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading='Filename', width=500)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_CREATE_PLAYLIST, name='create_playlist',
              parent=prnt, pos=wx.Point(541, 355), size=wx.Size(545, 434),
              style=wx.DEFAULT_DIALOG_STYLE, title='Create/Edit Playlist')
        self.SetClientSize(wx.Size(537, 405))
        self.Center(wx.BOTH)

        self.PLname = wx.TextCtrl(id=wxID_CREATE_PLAYLISTPLNAME, name='PLname',
              parent=self, pos=wx.Point(24, 40), size=wx.Size(424, 21), style=0,
              value='')

        self.staticText1 = wx.StaticText(id=wxID_CREATE_PLAYLISTSTATICTEXT1,
              label='Playlist file:', name='staticText1', parent=self,
              pos=wx.Point(24, 24), size=wx.Size(51, 13), style=0)

        self.listView1 = wx.ListCtrl(id=wxID_CREATE_PLAYLISTLISTVIEW1,
              name='listView1', parent=self, pos=wx.Point(24, 136),
              size=wx.Size(488, 216), style=wx.LC_HRULES | wx.LC_REPORT)
        self._init_coll_listView1_Columns(self.listView1)

        self.button2 = wx.Button(id=wxID_CREATE_PLAYLISTBUTTON2, label='Done',
              name='button2', parent=self, pos=wx.Point(456, 368),
              size=wx.Size(75, 23), style=0)
        self.button2.Bind(wx.EVT_BUTTON, self.OnOK, id=wxID_CREATE_PLAYLISTBUTTON2)

        self.staticLine1 = wx.StaticLine(id=wxID_CREATE_PLAYLISTSTATICLINE1,
              name='staticLine1', parent=self, pos=wx.Point(24, 84),
              size=wx.Size(488, 2), style=0)

        self.bitmapButton1 = wx.BitmapButton(bitmap=wx.Bitmap('images/NewItem.png',
              wx.BITMAP_TYPE_PNG), id=wxID_CREATE_PLAYLISTBITMAPBUTTON1,
              name='bitmapButton1', parent=self, pos=wx.Point(40, 96),
              size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.bitmapButton1.SetToolTipString('Add files to list')
        self.bitmapButton1.Bind(wx.EVT_BUTTON, self.OnAddfiles, id=wxID_CREATE_PLAYLISTBITMAPBUTTON1)

        self.bitmapButton2 = wx.BitmapButton(bitmap=wx.Bitmap('images/DeleteItem.png',
              wx.BITMAP_TYPE_PNG), id=wxID_CREATE_PLAYLISTBITMAPBUTTON2,
              name='bitmapButton2', parent=self, pos=wx.Point(72, 96),
              size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.bitmapButton2.SetToolTipString('Delete from list')
        self.bitmapButton2.Bind(wx.EVT_BUTTON, self.OnDeleteitem, id=wxID_CREATE_PLAYLISTBITMAPBUTTON2)

        self.bitmapButton3 = wx.BitmapButton(bitmap=wx.Bitmap('images/Open.png',
              wx.BITMAP_TYPE_PNG), id=wxID_CREATE_PLAYLISTBITMAPBUTTON3,
              name='bitmapButton3', parent=self, pos=wx.Point(456, 40),
              size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.bitmapButton3.Bind(wx.EVT_BUTTON, self.OnOpenPL, id=wxID_CREATE_PLAYLISTBITMAPBUTTON3)

        self.bitmapButton4 = wx.BitmapButton(bitmap=wx.Bitmap('images/Save.png',
              wx.BITMAP_TYPE_PNG), id=wxID_CREATE_PLAYLISTBITMAPBUTTON4,
              name='bitmapButton4', parent=self, pos=wx.Point(488, 40),
              size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.bitmapButton4.Bind(wx.EVT_BUTTON, self.OnSavePL, id=wxID_CREATE_PLAYLISTBITMAPBUTTON4)

        self.movedown = wx.BitmapButton(bitmap=wx.Bitmap('images/down.png',
              wx.BITMAP_TYPE_PNG), id=wxID_CREATE_PLAYLISTMOVEDOWN,
              name='movedown', parent=self, pos=wx.Point(154, 96),
              size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.movedown.SetToolTipString('Move down selected')
        self.movedown.Bind(wx.EVT_BUTTON, self.OnMoveDown, id=wxID_CREATE_PLAYLISTMOVEDOWN)

        self.moveup = wx.BitmapButton(bitmap=wx.Bitmap('images/up.png',
              wx.BITMAP_TYPE_PNG), id=wxID_CREATE_PLAYLISTMOVEUP, name='moveup',
              parent=self, pos=wx.Point(122, 96), size=wx.Size(24, 24),
              style=wx.BU_AUTODRAW)
        self.moveup.SetToolTipString('Move up selected')
        self.moveup.Bind(wx.EVT_BUTTON, self.OnMoveUp, id=wxID_CREATE_PLAYLISTMOVEUP)

    def __init__(self, parent):
        self._init_ctrls(parent)
        x,y=self.GetSizeTuple()
        self.SetPosition((wx.SystemSettings_GetMetric(wx.SYS_SCREEN_X)/2-x/2,wx.SystemSettings_GetMetric(wx.SYS_SCREEN_Y)/2-y/2))
        self.changed=False

    def OnOK(self, event):
        if self.changed:
            dlg = wx.MessageDialog(self, 'The Playlist is changed!\n Would you like to save?',
              'Caption', wx.YES | wx.NO | wx.ICON_INFORMATION)
            try:
                if dlg.ShowModal()==wx.ID_YES:
                    self.OnSavePL(None)

            finally:
                dlg.Destroy()
        self.Close()
        event.Skip()

    def OnCancel(self, event):
        event.Skip()

    def OnDeleteitem(self, event):
        idx=self.listView1.GetFirstSelected()
        while idx > -1:
            self.listView1.DeleteItem(idx)
            idx=self.listView1.GetFirstSelected()
            self.changed=True
        event.Skip()

    def OnOpenPL(self, event):
        dlg = wx.FileDialog(self, "Select file", ".", "", "*.m3u", wx.OPEN)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()

                plf=open(filename,'r')
                try:
                    self.PLname.SetValue(filename)
                    lines=plf.readlines()
                finally:
                    plf.close()

                self.listView1.DeleteAllItems()
                for line in lines:
                    x=self.listView1.GetItemCount()
                    self.listView1.InsertStringItem(x,line.strip('\n'))
        finally:
            dlg.Destroy()
        event.Skip()

    def OnSavePL(self, event):
        if self.PLname.GetLineLength(0) == 0:
            dlg = wx.FileDialog(self, "Select file", ".", "", "*.m3u", wx.SAVE)
            try:
                if dlg.ShowModal() == wx.ID_OK:
                    self.PLname.SetValue(dlg.GetPath())
                else:
                    return
            finally:
                dlg.Destroy()
        try:
            plf=open(self.PLname.GetValue(),'w')
            try:
                for i in range(0,self.listView1.GetItemCount()):
                    itemtext=self.listView1.GetItemText(i)
                    plf.write(itemtext+'\n')
            finally:
                plf.close()
                self.changed=False
        except Exception, e:
            print e
            return
        if event:
            event.Skip()

    def OnAddfiles(self, event):
        dlg = wx.FileDialog(self, "Select file", os.path.dirname(self.PLname.GetValue()), "", "*.*",wx.OPEN|wx.MULTIPLE)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                filenames = dlg.GetPaths()
                self.changed=True
                for filename in filenames:
                    x=self.listView1.GetItemCount()
                    self.listView1.InsertStringItem(x,os.path.basename(filename))
        finally:
            dlg.Destroy()
        event.Skip()

    def OnMoveUp(self, event):
        idx=self.listView1.GetFirstSelected()
        if idx > 0:
            itemtext=self.listView1.GetItemText(idx)
            self.listView1.DeleteItem(idx)
            self.listView1.InsertStringItem(idx-1,itemtext)
            self.changed=True
        event.Skip()

    def OnMoveDown(self, event):
        idx=self.listView1.GetFirstSelected()
        if idx < self.listView1.GetItemCount()-1:
            itemtext=self.listView1.GetItemText(idx)
            self.listView1.DeleteItem(idx)
            self.listView1.InsertStringItem(idx+1,itemtext)
            self.changed=True
        event.Skip()
