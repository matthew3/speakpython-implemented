#Boa:Frame:wxFrame1

import wx
import FilesDialog

def create(parent):
    return wxFrame1(parent)

[wxID_WXFRAME1, wxID_WXFRAME1ADD_BTN, wxID_WXFRAME1DEL_BTN, 
 wxID_WXFRAME1LISTCTRL1, wxID_WXFRAME1MOVE_DOWN_BTN, wxID_WXFRAME1MOVE_UP_BTN, 
 wxID_WXFRAME1PANEL1, wxID_WXFRAME1PL_TYPE, wxID_WXFRAME1STATICLINE1, 
] = [wx.NewId() for _init_ctrls in range(9)]

class wxFrame1(wx.Frame):
    def _init_coll_PLimageList_Images(self, parent):
        # generated method, don't edit

        parent.Add(bitmap=wx.Bitmap('images/null.png', wx.BITMAP_TYPE_PNG),
              mask=wx.NullBitmap)
        parent.Add(bitmap=wx.Bitmap('images/right.png', wx.BITMAP_TYPE_PNG),
              mask=wx.NullBitmap)

    def _init_coll_listCtrl1_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=' ',
              width=20)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading='Media file ', width=280)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading='Sub. file', width=280)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
              heading='Last Time (sec)', width=280)

    def _init_utils(self):
        # generated method, don't edit
        self.PLimageList = wx.ImageList(height=15, width=15)
        self._init_coll_PLimageList_Images(self.PLimageList)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_WXFRAME1, name='', parent=prnt,
              pos=wx.Point(542, 390), size=wx.Size(328, 170),
              style=wx.SIMPLE_BORDER, title='Playlist')
        self._init_utils()
        self.SetClientSize(wx.Size(320, 143))
        self.Bind(wx.EVT_DROP_FILES, self.OnWxFrame1DropFiles)

        self.panel1 = wx.Panel(id=wxID_WXFRAME1PANEL1, name='panel1',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(320, 143),
              style=wx.TAB_TRAVERSAL)

        self.staticLine1 = wx.StaticLine(id=wxID_WXFRAME1STATICLINE1,
              name='staticLine1', parent=self.panel1, pos=wx.Point(8, 104),
              size=wx.Size(304, 2), style=0)

        self.listCtrl1 = wx.ListCtrl(id=wxID_WXFRAME1LISTCTRL1,
              name='listCtrl1', parent=self.panel1, pos=wx.Point(8, 8),
              size=wx.Size(304, 88),
              style=wx.LC_VRULES | wx.LC_HRULES | wx.LC_REPORT)
        self._init_coll_listCtrl1_Columns(self.listCtrl1)
        self.listCtrl1.Bind(wx.EVT_LEFT_DCLICK, self.OnListCtrl1LeftDclick)

        self.add_btn = wx.BitmapButton(bitmap=wx.Bitmap('images/NewItem.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXFRAME1ADD_BTN, name='add_btn',
              parent=self.panel1, pos=wx.Point(16, 112), size=wx.Size(24, 24),
              style=wx.BU_AUTODRAW)
        self.add_btn.Bind(wx.EVT_BUTTON, self.OnAddButton,
              id=wxID_WXFRAME1ADD_BTN)

        self.del_btn = wx.BitmapButton(bitmap=wx.Bitmap('images/DeleteItem.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXFRAME1DEL_BTN, name='del_btn',
              parent=self.panel1, pos=wx.Point(56, 112), size=wx.Size(24, 24),
              style=wx.BU_AUTODRAW)
        self.del_btn.Bind(wx.EVT_BUTTON, self.OnDeleteButton,
              id=wxID_WXFRAME1DEL_BTN)

        self.move_down_btn = wx.BitmapButton(bitmap=wx.Bitmap('images/down.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXFRAME1MOVE_DOWN_BTN,
              name='move_down_btn', parent=self.panel1, pos=wx.Point(248, 112),
              size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.move_down_btn.Bind(wx.EVT_BUTTON, self.OnMoveDown,
              id=wxID_WXFRAME1MOVE_DOWN_BTN)

        self.move_up_btn = wx.BitmapButton(bitmap=wx.Bitmap('images/up.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXFRAME1MOVE_UP_BTN,
              name='move_up_btn', parent=self.panel1, pos=wx.Point(280, 112),
              size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.move_up_btn.Bind(wx.EVT_BUTTON, self.OnMoveUp,
              id=wxID_WXFRAME1MOVE_UP_BTN)

        self.pl_type = wx.Choice(choices=['Normal', 'Repeat All', 'Repeat One',
              'Random'], id=wxID_WXFRAME1PL_TYPE, name=u'pl_type',
              parent=self.panel1, pos=wx.Point(144, 114), size=wx.Size(90, 21),
              style=0)
        self.pl_type.SetSelection(0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.DragAcceptFiles(True)
        self.listCtrl1.AssignImageList(self.PLimageList,wx.IMAGE_LIST_SMALL)
        ##self.listCtrl1.SetImageList(self.PLimageList,wx.IMAGE_LIST_SMALL)
        self.lastpath=''
        self.loadPlayList()

    def OnAddButton(self, event):
        dlg=FilesDialog.create(self)
        try:
            if (self.lastpath):
                dlg.genericDirCtrl1.SetPath(self.lastpath)
            if dlg.ShowModal() == wx.ID_OK:
                fname=dlg.videofile_path.GetLabel()
                subname=dlg.subfile_path.GetLabel()
                self.appendItem(fname, subname)
                self.lastpath=dlg.genericDirCtrl1.GetPath()
        finally:
            dlg.Destroy()
        event.Skip()

    def OnDeleteButton(self, event):
        item=-1
        idx=[]
        while True:
            item= self.listCtrl1.GetNextItem(item,wx.LIST_NEXT_ALL,wx.LIST_STATE_SELECTED)
            if item == -1:
                break
            idx.append(item)
        idx.reverse()
        for x in idx:
            self.listCtrl1.DeleteItem(x)
        event.Skip()

    def OnListCtrl1ListItemSelected(self, event):
        event.Skip()

    def OnListCtrl1LeftDclick(self, event):
        item=self.listCtrl1.GetNextItem(-1,wx.LIST_NEXT_ALL,wx.LIST_STATE_SELECTED)
        if item != -1:
            prnt=self.GetParent()
            time=self.getCellContentsString(item, 3)
            prnt.OnStop(None)
            prnt.cur_item=item
            prnt.currentsec = float(time)
            prnt.OnPlay(None)
        event.Skip()

    def OnWxFrame1DropFiles(self, event):
        fnames=event.GetFiles()
        for fname in fnames:
            self.appendItem(fname)
        event.Skip()

    def appendItem(self, fname='', subtitle='', time='0'):
        cnt=self.listCtrl1.GetItemCount()
        for row in range(cnt):
            dst = self.getCellContentsString(row, 1)
            if fname==dst: #file already in list
                return
        self.listCtrl1.InsertImageItem(cnt,0)
        self.listCtrl1.SetStringItem(cnt,1,fname)
        self.listCtrl1.SetStringItem(cnt,2,subtitle)
        self.listCtrl1.SetStringItem(cnt,3,time)

    def OnMoveDown(self, event):
        item=self.listCtrl1.GetNextItem(-1,wx.LIST_NEXT_ALL,wx.LIST_STATE_SELECTED)
        if item != -1:
            c0=self.listCtrl1.GetItem(item)
            c1=self.listCtrl1.GetItem(item,1)
            c2=self.listCtrl1.GetItem(item,2)
            if c0.GetId() < self.listCtrl1.GetItemCount()-1 :
                self.listCtrl1.DeleteItem(item)
                c0.SetId(item+1)
                c1.SetId(item+1)
                c2.SetId(item+1)
                c0.SetState(0)
                c1.SetState(0)
                c2.SetState(0)
                self.listCtrl1.InsertItem(c0)
                self.listCtrl1.SetItem(c1)
                self.listCtrl1.SetItem(c2)
        event.Skip()

    def OnMoveUp(self, event):
        item=self.listCtrl1.GetNextItem(-1,wx.LIST_NEXT_ALL,wx.LIST_STATE_SELECTED)
        if item != -1:
            c0=self.listCtrl1.GetItem(item)
            c1=self.listCtrl1.GetItem(item,1)
            c2=self.listCtrl1.GetItem(item,2)
            if c0.GetId()>0:
                self.listCtrl1.DeleteItem(item)
                c0.SetId(item-1)
                c1.SetId(item-1)
                c2.SetId(item-1)
                c0.SetState(0)
                c1.SetState(0)
                c2.SetState(0)
                self.listCtrl1.InsertItem(c0)
                self.listCtrl1.SetItem(c1)
                self.listCtrl1.SetItem(c2)
        event.Skip()

    def savePlayList(self):
        import codecs
        plf = codecs.open('playlist.pls', 'wb', encoding='utf-8')
        cnt = self.listCtrl1.GetItemCount()
        prnt=self.GetParent()
        for i in range(cnt):
            filename = self.getCellContentsString(i, 1).strip()
            subtitle = self.getCellContentsString(i, 2).strip()
            time = self.getCellContentsString(i, 3).strip()

            plf.write(filename+'\t'+subtitle+'\t'+time+'\n')
        plf.close()

    def loadPlayList(self):
        import codecs
        try:
            plf = codecs.open('playlist.pls', 'rb', encoding='utf-8').readlines()
            ##plf.reverse()
            cnt=self.listCtrl1.GetItemCount()
            for line in plf:
                if len(line.strip())>0:
                    #~ self.listCtrl1.InsertStringItem(cnt,' ')
                    filename,subtitle,time = line.split('\t')
                    self.appendItem(filename, subtitle, time[:-1])
                    #~ self.listCtrl1.SetStringItem(cnt,1,filename.strip())
                    #~ self.listCtrl1.SetStringItem(cnt,2,subtitle.strip())
                    #~ self.listCtrl1.SetStringItem(cnt,3,time.strip())
        except IOError, e:
            return

    def getCellContentsString(self, row_number, column):
       return self.listCtrl1.GetItem(row_number, column).GetText()

    def refreshTime(self):
        prnt=self.GetParent()
        filename = prnt.fname[1:-1]
        for row in range(self.listCtrl1.GetItemCount()):
            file = self.getCellContentsString(row, 1)
            if file==filename:
                self.listCtrl1.SetStringItem(row, 3, str(prnt.currentsec))
                break

    def OnChoice1Choice(self, event):
        event.Skip()
