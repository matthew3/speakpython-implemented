import asyncore,socket
from threading import *

class winlirc_client(asyncore.dispatcher):
    def __init__(self,host='127.0.0.1',port=8765):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.lines=[]
        self.thrd=None
        addr=(host,port)
        self.socket.setblocking(1)
        self.thrd=None
        try:
            self.connect(addr)
        except Exception, e:
            print e
            pass
        ##self.socket.setblocking(0)
        ##print 'winlirc client %s\n' % str(self.connected)
        if self.connected:
            self.thrd=Thread(target=asyncore.loop,name='winlirc-thread')
            self.thrd.setDaemon(True)
            self.thrd.start()

    def handle_connect(self):
        ##print '>>connected<<\n'
        pass

    def handle_close(self):
        self.close()
        self.connected=False
        ##print '>>disconnected<<\n'

    def handle_read(self):
        if len(self.lines) < 128:
            line=self.recv(128)
            self.lines.append(line)
        ##print 'line:: %s ::\n' % line

    def handle_write(self):
        pass

    def log(self):
        pass

    def writable(self):
        return False

    def getline(self):
        if self.connected == True and len(self.lines) > 0:
            line=self.lines[0]
            self.lines.remove(self.lines[0])
            return line

        return ''

    def getButtonName(self):
        if self.connected == True and len(self.lines) > 0:
            line=self.lines[0]
            self.lines.remove(self.lines[0])
            ltoken=line.split()
            if len(ltoken)==4:
                return ltoken[2]

        return ''

    def getButton(self):
        if self.connected == True and len(self.lines) > 0:
            line=self.lines[0]
            self.lines.remove(self.lines[0])
            ltoken=line.split()
            if len(ltoken)==4:
                return (int(ltoken[1],16),ltoken[2])

        return (-1,'')