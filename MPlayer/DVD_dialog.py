#Boa:Dialog:DVD_open_Dialog

import wx
import struct
import win32api
import string
import os

def create(parent):
    return DVD_open_Dialog(parent)

[wxID_DVD_OPEN_DIALOG, wxID_DVD_OPEN_DIALOGALANG,
 wxID_DVD_OPEN_DIALOGCANCELBUTTON, wxID_DVD_OPEN_DIALOGGENERICDIRCTRL1,
 wxID_DVD_OPEN_DIALOGNUMANG, wxID_DVD_OPEN_DIALOGNUMCHAPT,
 wxID_DVD_OPEN_DIALOGOKBUTTON, wxID_DVD_OPEN_DIALOGSLANG,
 wxID_DVD_OPEN_DIALOGSTATICLINE1, wxID_DVD_OPEN_DIALOGSTATICLINE2,
 wxID_DVD_OPEN_DIALOGSTATICTEXT1, wxID_DVD_OPEN_DIALOGSTATICTEXT2,
 wxID_DVD_OPEN_DIALOGSTATICTEXT3, wxID_DVD_OPEN_DIALOGSTATICTEXT4,
 wxID_DVD_OPEN_DIALOGTITLENUM,
] = [wx.NewId() for _init_ctrls in range(15)]

class DVD_open_Dialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_DVD_OPEN_DIALOG, name='DVD_open_Dialog',
              parent=prnt, pos=wx.Point(492, 383), size=wx.Size(401, 340),
              style=wx.DEFAULT_DIALOG_STYLE, title='Open DVD')
        self.SetClientSize(wx.Size(393, 311))

        self.staticText1 = wx.StaticText(id=wxID_DVD_OPEN_DIALOGSTATICTEXT1,
              label='Select DVD device or directory:', name='staticText1',
              parent=self, pos=wx.Point(24, 16), size=wx.Size(149, 13), style=0)

        self.staticText2 = wx.StaticText(id=wxID_DVD_OPEN_DIALOGSTATICTEXT2,
              label='Select Movie Title:', name='staticText2', parent=self,
              pos=wx.Point(232, 16), size=wx.Size(88, 13), style=0)
        self.staticText2.Enable(False)

        self.staticText3 = wx.StaticText(id=wxID_DVD_OPEN_DIALOGSTATICTEXT3,
              label='Select Audio Language:', name='staticText3', parent=self,
              pos=wx.Point(232, 112), size=wx.Size(114, 13), style=0)
        self.staticText3.Enable(False)

        self.staticText4 = wx.StaticText(id=wxID_DVD_OPEN_DIALOGSTATICTEXT4,
              label='Select Subtitles Language:', name='staticText4',
              parent=self, pos=wx.Point(232, 168), size=wx.Size(127, 13),
              style=0)
        self.staticText4.Enable(False)

        self.staticLine1 = wx.StaticLine(id=wxID_DVD_OPEN_DIALOGSTATICLINE1,
              name='staticLine1', parent=self, pos=wx.Point(8, 256),
              size=wx.Size(376, 2), style=0)

        self.alang = wx.ComboBox(choices=[], id=wxID_DVD_OPEN_DIALOGALANG,
              name='alang', parent=self, pos=wx.Point(248, 136),
              size=wx.Size(80, 21), style=0, value='')
        self.alang.SetLabel('')
        self.alang.Enable(False)

        self.slang = wx.ComboBox(choices=[], id=wxID_DVD_OPEN_DIALOGSLANG,
              name='slang', parent=self, pos=wx.Point(248, 192),
              size=wx.Size(80, 21), style=0, value='')
        self.slang.SetLabel('')
        self.slang.Enable(False)

        self.OKButton = wx.Button(id=wxID_DVD_OPEN_DIALOGOKBUTTON, label='OK',
              name='OKButton', parent=self, pos=wx.Point(64, 272),
              size=wx.Size(75, 23), style=0)
        self.OKButton.Enable(False)
        self.OKButton.Bind(wx.EVT_BUTTON, self.OnOKButtonButton,
              id=wxID_DVD_OPEN_DIALOGOKBUTTON)

        self.CancelButton = wx.Button(id=wxID_DVD_OPEN_DIALOGCANCELBUTTON,
              label='Cancel', name='CancelButton', parent=self,
              pos=wx.Point(252, 272), size=wx.Size(75, 23), style=0)
        self.CancelButton.Bind(wx.EVT_BUTTON, self.OnCancelButtonButton,
              id=wxID_DVD_OPEN_DIALOGCANCELBUTTON)

        self.staticLine2 = wx.StaticLine(id=wxID_DVD_OPEN_DIALOGSTATICLINE2,
              name='staticLine2', parent=self, pos=wx.Point(192, 24),
              size=wx.Size(2, 216), style=wx.LI_VERTICAL)

        self.titlenum = wx.Choice(choices=[], id=wxID_DVD_OPEN_DIALOGTITLENUM,
              name='titlenum', parent=self, pos=wx.Point(248, 40),
              size=wx.Size(80, 21), style=0)
        self.titlenum.Enable(False)
        self.titlenum.Bind(wx.EVT_CHOICE, self.OnTitlenumChoice,
              id=wxID_DVD_OPEN_DIALOGTITLENUM)

        self.numang = wx.StaticText(id=wxID_DVD_OPEN_DIALOGNUMANG,
              label='0 angles', name='numang', parent=self, pos=wx.Point(248,
              70), size=wx.Size(40, 13), style=0)
        self.numang.SetFont(wx.Font(8,wx.SWISS,wx.ITALIC,wx.NORMAL, False,
              'MS Shell Dlg'))
        self.numang.Enable(False)

        self.numchapt = wx.StaticText(id=wxID_DVD_OPEN_DIALOGNUMCHAPT,
              label='0 chapters', name='numchapt', parent=self,
              pos=wx.Point(296, 70), size=wx.Size(50, 13), style=0)
        self.numchapt.SetFont(wx.Font(8, wx.SWISS, wx.ITALIC, wx.NORMAL, False,
              'MS Shell Dlg'))
        self.numchapt.Enable(False)

        self.genericDirCtrl1 = wx.GenericDirCtrl(defaultFilter=0, dir='.',
              filter='', id=wxID_DVD_OPEN_DIALOGGENERICDIRCTRL1,
              name='genericDirCtrl1', parent=self, pos=wx.Point(24, 40),
              size=wx.Size(160, 192),
              style=wx.DIRCTRL_DIR_ONLY | wx.SUNKEN_BORDER)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.Bind(wx.EVT_TREE_SEL_CHANGED, self.OnDirChanged, id=self.genericDirCtrl1.GetTreeCtrl().GetId())
        x,y=self.GetSizeTuple()
        self.SetPosition((wx.SystemSettings_GetMetric(wx.SYS_SCREEN_X)/2-x/2,wx.SystemSettings_GetMetric(wx.SYS_SCREEN_Y)/2-y/2))
        self.vts_ttn=[]
        self.prefered_alang='en'
        self.prefered_slang=''
        self.dvd_dev=''

    def OnOKButtonButton(self, event):
        self.EndModal(wx.ID_OK)
        event.Skip()

    def OnCancelButtonButton(self, event):
        self.EndModal(wx.ID_CANCEL)
        event.Skip()

    def OnTitlenumChoice(self, event):
        self.parse_VTS_IFO()
        event.Skip()

    def parse_VTS_IFO(self):
        self.alang.Enable(False)
        self.alang.Clear()
        self.slang.Enable(False)
        self.slang.Clear()
        self.staticText3.Enable(False)
        self.staticText4.Enable(False)

        ifo=None
        try:
##            dvd_dev=self.devicelist.GetStringSelection()
            self.dvd_dev=self.genericDirCtrl1.GetPath()
            if self.dvd_dev[-1]=='\\':
                self.dvd_dev=self.dvd_dev[:-1]
            sel=self.titlenum.GetSelection()
            (ttype,numa,numc,pmmask,vtsnum,ttn,vtsstartsec)=struct.unpack('>bbhhbbl',self.vts_ttn[sel])
            ##print str(ttn)+'\n'
            self.numang.SetLabel(str(numa)+' angles')
            self.numchapt.SetLabel(str(numc)+' chapters')
            num='%02d' % vtsnum
            if os.path.isdir(self.dvd_dev+'\\VIDEO_TS') :
                ifoname=self.dvd_dev+'\\VIDEO_TS\\VTS_'+num+'_0.IFO'
            else:
                ifoname=self.dvd_dev+'\\VTS_'+num+'_0.IFO'
            ifo=open(ifoname)
            #parse audio attr.
            ifo.seek(0x202)
            tmp=ifo.read(2)
            nums_of_audio,=struct.unpack('>h',tmp)
            ##print 'number of audio: '+str(nums_of_audio)+'\n'
            for i in range(nums_of_audio):
                addr=0x204+i*8+2
                ifo.seek(addr)
                tmp=ifo.read(2)
                self.alang.Append(tmp)
                if tmp == self.prefered_alang:
                    self.alang.SetStringSelection(tmp)
            self.alang.Enable(True)
            self.staticText3.Enable(True)
            #parse subt. attr.
            ifo.seek(0x254)
            tmp=ifo.read(2)
            nums_of_sub,=struct.unpack('>h',tmp)
            ##print 'number of sub: '+str(nums_of_sub)+'\n'
            for i in range(nums_of_sub):
                addr=0x256+i*6+2
                ifo.seek(addr)
                tmp=ifo.read(2)
                self.slang.Append(tmp)
                if tmp == self.prefered_slang:
                    self.slang.SetStringSelection(tmp)
            self.slang.Enable(True)
            self.staticText4.Enable(True)

            ifo.close()

        except Exception, e:
            ##print e
            if ifo:
                ifo.close()
            self.alang.Enable(False)
            self.staticText3.Enable(False)
            self.alang.Clear()
            self.slang.Enable(False)
            self.staticText4.Enable(False)
            self.alang.Clear()
            self.OKButton.Enable(False)
            dlg = wx.MessageDialog(self, 'Oops, an error occurred when parsing DVD.',
                  'Error', wx.OK | wx.ICON_ERROR)
            try:
                dlg.ShowModal()
            finally:
                dlg.Destroy()

    def OnDirChanged(self,event):
        self.staticText4.Enable(False)
        self.staticText2.Enable(False)
        self.staticText3.Enable(False)
        self.titlenum.Enable(False)
        self.titlenum.Clear()
        self.alang.Enable(False)
        self.alang.Clear()
        self.slang.Enable(False)
        self.slang.Clear()

        self.dvd_dev=self.genericDirCtrl1.GetPath()
        if self.dvd_dev[-1]=='\\':
            self.dvd_dev=self.dvd_dev[:-1]
        ifo=None
        try:

            if os.path.isdir(self.dvd_dev+'\\VIDEO_TS') :
                ifo=open(self.dvd_dev+'\\VIDEO_TS\\VIDEO_TS.IFO','rb')
            else:
                ifo=open(self.dvd_dev+'\\VIDEO_TS.IFO','rb')
            #
            ifo.seek(0x3E)
            tmp=ifo.read(2)
            vts,=struct.unpack('>h',tmp)
            #
            ifo.seek(0xc4)
            tmp=ifo.read(4)
            #
            tt,=struct.unpack('>l',tmp)
            #
            ifo.seek(tt*2048)
            tmp=ifo.read(2)
            ifo.seek(6,1)
            nums_of_title,=struct.unpack('>h',tmp)
            self.vts_ttn=[]
            for i in range(1,nums_of_title+1):
                tmp=ifo.read(12)
                self.vts_ttn.append(tmp)
                self.titlenum.Append('title '+str(i))
            ##print 'number of title: '+str(nums_of_title)+'\n'
            self.titlenum.Enable(True)
            self.staticText2.Enable(True)
            ifo.close()
            self.OKButton.Enable(True)
            self.titlenum.SetSelection(0)
            self.parse_VTS_IFO()
        except Exception, e:
            ##print e
            if ifo:
                ifo.close()
            self.titlenum.Enable(False)
            self.staticText2.Enable(False)
            self.titlenum.Clear()
            self.OKButton.Enable(False)

        event.Skip()