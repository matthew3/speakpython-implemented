
About
------
MPlayer Control (MPlayerC) is a GUI for MPlayer Movie Player (www.mplayerhq.hu)
Writed in Python, using wxPython package.

More info found in help directory.


License
-------
MPlayer Control relesed under GNU-GPL. See the license.txt.


Author
------
The main developer: Tamas Zelena 
