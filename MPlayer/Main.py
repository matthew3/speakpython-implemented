#Boa:Frame:wxMain
#~ -ss use to seek to
##///////////////////////////////////////////////////////////////////////////
## Name:        Main.py
## Purpose:     Main window frame of MPlayer Control
## Author:      Tamas Zelena
## Modified by:
## Created:     09/07/2004
## Copyright:   (c) Tamas Zelena
## Licence:     GNU-GPL
##///////////////////////////////////////////////////////////////////////////

import wx
import wx.lib.buttons
import os,sys,time
import ctypes
import Settings
import winlirc
import AboutDialog
import FilesDialog
import string
import win32ui,win32gui,win32api,win32con
import struct,array
import traceback
import DVD_dialog
import EQFrame
import PListFrame

PLAY_FILE=0
PLAY_DVD=1
PLAY_LIST=2
PLAY_URL=3

#~ TODO read from config file
EXEPATH = r'mplayer\\mplayer.exe'

def create(parent):
    return wxMain(parent)

[wxID_WXMAIN, wxID_WXMAINAUDIOTYPETEXT, wxID_WXMAINFILENAME, 
 wxID_WXMAINGAUGE1, wxID_WXMAININFOCHECK, wxID_WXMAININFOTEXT, 
 wxID_WXMAINPANEL1, wxID_WXMAINPANEL2, wxID_WXMAINSTATICBITMAP1, 
 wxID_WXMAINSTATICBITMAP10, wxID_WXMAINSTATICBITMAP11, 
 wxID_WXMAINSTATICBITMAP12, wxID_WXMAINSTATICBITMAP13, 
 wxID_WXMAINSTATICBITMAP2, wxID_WXMAINSTATICBITMAP3, wxID_WXMAINSTATICBITMAP4, 
 wxID_WXMAINSTATICBITMAP5, wxID_WXMAINSTATICBITMAP6, wxID_WXMAINSTATICBITMAP7, 
 wxID_WXMAINSTATICBITMAP8, wxID_WXMAINSTATICBITMAP9, wxID_WXMAINSTATICLINE1, 
 wxID_WXMAINSTATICTEXT1, wxID_WXMAINSTATICTEXT3, wxID_WXMAINSTATUSTEXT, 
 wxID_WXMAINTIMEPOS, wxID_WXMAINTYPETEXT, 
] = [wx.NewId() for _init_ctrls in range(27)]

[wxID_WXMAINTIMER1, wxID_WXMAINTIMER2, wxID_WXMAINTIMER3, 
] = [wx.NewId() for _init_utils in range(3)]

[wxID_WXMAINPOPUPMENUITEMS0, wxID_WXMAINPOPUPMENUITEMS1, 
 wxID_WXMAINPOPUPMENUITEMS10, wxID_WXMAINPOPUPMENUITEMS12, 
 wxID_WXMAINPOPUPMENUITEMS13, wxID_WXMAINPOPUPMENUITEMS15, 
 wxID_WXMAINPOPUPMENUITEMS16, wxID_WXMAINPOPUPMENUITEMS17, 
 wxID_WXMAINPOPUPMENUITEMS18, wxID_WXMAINPOPUPMENUITEMS19, 
 wxID_WXMAINPOPUPMENUITEMS2, wxID_WXMAINPOPUPMENUITEMS20, 
 wxID_WXMAINPOPUPMENUITEMS21, wxID_WXMAINPOPUPMENUITEMS5, 
 wxID_WXMAINPOPUPMENUITEMS6, wxID_WXMAINPOPUPMENUITEMS7, 
 wxID_WXMAINPOPUPMENUITEMS8, 
] = [wx.NewId() for _init_coll_popupmenu_Items in range(17)]

[wxID_WXMAINPLAYLISTMENUITEMS0, wxID_WXMAINPLAYLISTMENUITEMS1,
] = [wx.NewId() for _init_coll_playlistmenu_Items in range(2)]

def Boolean(text):
    if text=='True': return True
    if text=='False': return False
    raise Exception, 'Boolean error'

[wxID_WXMAINHISTORYMENUITEMS0] = [wx.NewId() for _init_coll_historymenu_Items in range(1)]

class wxMain(wx.Frame):
    def _init_coll_popupmenu_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS10,
              kind=wx.ITEM_NORMAL, text='Open File')
        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS15,
              kind=wx.ITEM_NORMAL, text='Open DVD')
        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS21,
              kind=wx.ITEM_NORMAL, text=u'Open URL')
        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS16,
              kind=wx.ITEM_NORMAL, text='Playlist On/Off')
        parent.AppendMenu(help='', id=wxID_WXMAINPOPUPMENUITEMS12,
              submenu=self.historymenu, text='History')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS5,
              kind=wx.ITEM_NORMAL, text='Play')
        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS7,
              kind=wx.ITEM_NORMAL, text='Pause on/off')
        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS6,
              kind=wx.ITEM_NORMAL, text='Stop')
        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS17,
              kind=wx.ITEM_NORMAL, text='Prev.')
        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS18,
              kind=wx.ITEM_NORMAL, text='Next')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS8,
              kind=wx.ITEM_NORMAL, text='Mute on/off')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS13,
              kind=wx.ITEM_NORMAL, text='Fullscreen On/Off')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS20,
              kind=wx.ITEM_NORMAL, text='Equalizer On/Off')
        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS19,
              kind=wx.ITEM_NORMAL, text='Settings...')
        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS2,
              kind=wx.ITEM_NORMAL, text='Help')
        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS1,
              kind=wx.ITEM_NORMAL, text='About')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_WXMAINPOPUPMENUITEMS0,
              kind=wx.ITEM_NORMAL, text='Exit')
        self.Bind(wx.EVT_MENU, self.OnClose, id=wxID_WXMAINPOPUPMENUITEMS0)
        self.Bind(wx.EVT_MENU, self.OnMute, id=wxID_WXMAINPOPUPMENUITEMS8)
        self.Bind(wx.EVT_MENU, self.OnStop, id=wxID_WXMAINPOPUPMENUITEMS6)
        self.Bind(wx.EVT_MENU, self.OnPause, id=wxID_WXMAINPOPUPMENUITEMS7)
        self.Bind(wx.EVT_MENU, self.OnPlay, id=wxID_WXMAINPOPUPMENUITEMS5)
        self.Bind(wx.EVT_MENU, self.OnOpenFile, id=wxID_WXMAINPOPUPMENUITEMS10)
        self.Bind(wx.EVT_MENU, self.OnFullscreen,
              id=wxID_WXMAINPOPUPMENUITEMS13)
        self.Bind(wx.EVT_MENU, self.OnAbout, id=wxID_WXMAINPOPUPMENUITEMS1)
        self.Bind(wx.EVT_MENU, self.OnHelp, id=wxID_WXMAINPOPUPMENUITEMS2)
        self.Bind(wx.EVT_MENU, self.OnOpenDVD, id=wxID_WXMAINPOPUPMENUITEMS15)
        self.Bind(wx.EVT_MENU, self.OnNext, id=wxID_WXMAINPOPUPMENUITEMS18)
        self.Bind(wx.EVT_MENU, self.OnPrev, id=wxID_WXMAINPOPUPMENUITEMS17)
        self.Bind(wx.EVT_MENU, self.OnSettings, id=wxID_WXMAINPOPUPMENUITEMS19)
        self.Bind(wx.EVT_MENU, self.OnEQ, id=wxID_WXMAINPOPUPMENUITEMS20)
        self.Bind(wx.EVT_MENU, self.OnPlaylist, id=wxID_WXMAINPOPUPMENUITEMS16)
        self.Bind(wx.EVT_MENU, self.OnOpenURL, id=wxID_WXMAINPOPUPMENUITEMS21)

    def _init_coll_historymenu_Items(self, parent):
        # generated method, don't edit

        parent.AppendSeparator()
        parent.Append(help='', id=wxID_WXMAINHISTORYMENUITEMS0,
              kind=wx.ITEM_NORMAL, text='Clear')
        self.Bind(wx.EVT_MENU, self.OnHistoryClear,
              id=wxID_WXMAINHISTORYMENUITEMS0)

    def _init_utils(self):
        # generated method, don't edit
        self.timer1 = wx.Timer(id=wxID_WXMAINTIMER1, owner=self)
        self.Bind(wx.EVT_TIMER, self.OnTimer1Timer, id=wxID_WXMAINTIMER1)

        self.timer2 = wx.Timer(id=wxID_WXMAINTIMER2, owner=self)
        self.Bind(wx.EVT_TIMER, self.OnTimer2Timer, id=wxID_WXMAINTIMER2)

        self.timer3 = wx.Timer(id=wxID_WXMAINTIMER3, owner=self)
        self.Bind(wx.EVT_TIMER, self.OnTimer3Timer, id=wxID_WXMAINTIMER3)

        self.popupmenu = wx.Menu(title='')
        self.popupmenu.SetEvtHandlerEnabled(2)

        self.historymenu = wx.Menu(title='')

        self._init_coll_popupmenu_Items(self.popupmenu)
        self._init_coll_historymenu_Items(self.historymenu)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_WXMAIN, name='wxMain', parent=prnt,
              pos=wx.Point(620, 439), size=wx.Size(387, 266),
              style=wx.THICK_FRAME | wx.DEFAULT_DIALOG_STYLE,
              title='MPlayer Control')
        self._init_utils()
        self.SetClientSize(wx.Size(379, 239))
        self.SetIcon(wx.Icon('images/mplayerc.ico',wx.BITMAP_TYPE_ICO))
        self.Center(wx.BOTH)
        self.Bind(wx.EVT_SIZE, self.OnWxMainSize)
        self.Bind(wx.EVT_CLOSE, self.OnWxMainClose)
        self.Bind(wx.EVT_DROP_FILES, self.OnWxMainDropFiles)
        self.Bind(wx.EVT_MOVE, self.OnWxMainMove)

        self.panel1 = wx.Panel(id=wxID_WXMAINPANEL1, name='panel1', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(379, 239), style=0)
        self.panel1.SetBackgroundColour(wx.Colour(212, 208, 200))
        self.panel1.SetBackgroundStyle(wx.BG_STYLE_SYSTEM)
        self.panel1.Bind(wx.EVT_RIGHT_DOWN, self.OnPopupMain)

        self.panel2 = wx.Panel(id=wxID_WXMAINPANEL2, name='panel2',
              parent=self.panel1, pos=wx.Point(40, 8), size=wx.Size(304, 56),
              style=wx.SUNKEN_BORDER)
        self.panel2.SetBackgroundColour(wx.Colour(85, 85, 85))
        self.panel2.Bind(wx.EVT_RIGHT_DOWN, self.OnPopup)
        self.panel2.Bind(wx.EVT_KEY_DOWN, self.OnPanel2KeyDown)

        self.staticText1 = wx.StaticText(id=wxID_WXMAINSTATICTEXT1,
              label='State:', name='staticText1', parent=self.panel2,
              pos=wx.Point(16, 24), size=wx.Size(28, 13), style=0)
        self.staticText1.SetForegroundColour(wx.Colour(57, 187, 198))
        self.staticText1.SetBackgroundColour(wx.Colour(85, 85, 85))
        self.staticText1.Bind(wx.EVT_RIGHT_DOWN, self.OnPopup)

        self.staticBitmap3 = wx.StaticBitmap(bitmap=wx.Bitmap('images/folder-small.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXMAINSTATICBITMAP3,
              name='staticBitmap3', parent=self.panel1, pos=wx.Point(288, 72),
              size=wx.Size(24, 24), style=0)
        self.staticBitmap3.SetToolTipString('Open Media File')
        self.staticBitmap3.Bind(wx.EVT_LEFT_UP, self.OnOpenFile)
        self.staticBitmap3.Bind(wx.EVT_RIGHT_DOWN, self.OnPopupMain)
        self.staticBitmap3.Bind(wx.EVT_LEFT_DOWN, self.OnBitmapLeftDown)

        self.staticBitmap4 = wx.StaticBitmap(bitmap=wx.Bitmap('images/mute-small.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXMAINSTATICBITMAP4,
              name='staticBitmap4', parent=self.panel1, pos=wx.Point(32, 69),
              size=wx.Size(24, 25), style=0)
        self.staticBitmap4.SetToolTipString('Mute on')
        self.staticBitmap4.Bind(wx.EVT_LEFT_UP, self.OnMute)
        self.staticBitmap4.Bind(wx.EVT_RIGHT_DOWN, self.OnPopupMain)
        self.staticBitmap4.Bind(wx.EVT_LEFT_DOWN, self.OnBitmapLeftDown)

        self.gauge1 = wx.Gauge(id=wxID_WXMAINGAUGE1, name='gauge1',
              parent=self.panel2, pos=wx.Point(16, 40), range=100,
              size=wx.Size(272, 8), style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.gauge1.SetValue(0)
        self.gauge1.SetLabel('')
        self.gauge1.SetForegroundColour(wx.Colour(65, 175, 188))
        self.gauge1.SetToolTipString('0%')
        self.gauge1.Bind(wx.EVT_LEFT_DOWN, self.OnGauge1LeftDown)
        self.gauge1.Bind(wx.EVT_RIGHT_DOWN, self.OnPopup)

        self.staticBitmap2 = wx.StaticBitmap(bitmap=wx.Bitmap('images/setting-small.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXMAINSTATICBITMAP2,
              name='staticBitmap2', parent=self.panel1, pos=wx.Point(328, 72),
              size=wx.Size(24, 24), style=0)
        self.staticBitmap2.SetToolTipString('Settings')
        self.staticBitmap2.Bind(wx.EVT_LEFT_UP, self.OnSettings)
        self.staticBitmap2.Bind(wx.EVT_RIGHT_DOWN, self.OnPopupMain)
        self.staticBitmap2.Bind(wx.EVT_LEFT_DOWN, self.OnBitmapLeftDown)

        self.staticBitmap5 = wx.StaticBitmap(bitmap=wx.Bitmap('images/play-small.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXMAINSTATICBITMAP5,
              name='staticBitmap5', parent=self.panel1, pos=wx.Point(152, 72),
              size=wx.Size(24, 24), style=0)
        self.staticBitmap5.SetToolTipString('Play Movie')
        self.staticBitmap5.Bind(wx.EVT_LEFT_UP, self.OnPlay)
        self.staticBitmap5.Bind(wx.EVT_RIGHT_DOWN, self.OnPopupMain)
        self.staticBitmap5.Bind(wx.EVT_LEFT_DOWN, self.OnBitmapLeftDown)

        self.filename = wx.StaticText(id=wxID_WXMAINFILENAME, label='',
              name='filename', parent=self.panel2, pos=wx.Point(48, 8),
              size=wx.Size(240, 13), style=0)
        self.filename.SetForegroundColour(wx.Colour(57, 187, 198))
        self.filename.SetBackgroundColour(wx.Colour(85, 85, 85))
        self.filename.Bind(wx.EVT_RIGHT_DOWN, self.OnPopup)

        self.timepos = wx.StaticText(id=wxID_WXMAINTIMEPOS, label='  00:00:00',
              name='timepos', parent=self.panel2, pos=wx.Point(240, 24),
              size=wx.Size(48, 13), style=wx.ALIGN_RIGHT)
        self.timepos.SetForegroundColour(wx.Colour(57, 187, 198))
        self.timepos.SetBackgroundColour(wx.Colour(85, 85, 85))
        self.timepos.SetToolTipString('Elapsed time')
        self.timepos.Bind(wx.EVT_RIGHT_DOWN, self.OnPopup)
        self.timepos.Bind(wx.EVT_LEFT_DOWN, self.OnTimeposLeftDown)

        self.staticText3 = wx.StaticText(id=wxID_WXMAINSTATICTEXT3,
              label='File:', name='staticText3', parent=self.panel2,
              pos=wx.Point(16, 8), size=wx.Size(24, 13), style=0)
        self.staticText3.SetForegroundColour(wx.Colour(57, 187, 198))
        self.staticText3.SetBackgroundColour(wx.Colour(85, 85, 85))
        self.staticText3.Bind(wx.EVT_RIGHT_DOWN, self.OnPopup)

        self.staticBitmap6 = wx.StaticBitmap(bitmap=wx.Bitmap('images/stop-small.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXMAINSTATICBITMAP6,
              name='staticBitmap6', parent=self.panel1, pos=wx.Point(112, 72),
              size=wx.Size(24, 24), style=0)
        self.staticBitmap6.SetToolTipString('Stop playing')
        self.staticBitmap6.Bind(wx.EVT_LEFT_UP, self.OnStop)
        self.staticBitmap6.Bind(wx.EVT_RIGHT_DOWN, self.OnPopupMain)
        self.staticBitmap6.Bind(wx.EVT_LEFT_DOWN, self.OnBitmapLeftDown)

        self.staticBitmap7 = wx.StaticBitmap(bitmap=wx.Bitmap('images/pause-small.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXMAINSTATICBITMAP7,
              name='staticBitmap7', parent=self.panel1, pos=wx.Point(72, 72),
              size=wx.Size(24, 24), style=0)
        self.staticBitmap7.SetToolTipString('Pause playing')
        self.staticBitmap7.Bind(wx.EVT_LEFT_UP, self.OnPause)
        self.staticBitmap7.Bind(wx.EVT_RIGHT_DOWN, self.OnPopupMain)
        self.staticBitmap7.Bind(wx.EVT_LEFT_DOWN, self.OnBitmapLeftDown)

        self.staticLine1 = wx.StaticLine(id=wxID_WXMAINSTATICLINE1,
              name='staticLine1', parent=self.panel1, pos=wx.Point(8, 104),
              size=wx.Size(348, 2), style=0)

        self.infocheck = wx.CheckBox(id=wxID_WXMAININFOCHECK, label='',
              name='infocheck', parent=self.panel1, pos=wx.Point(356, 98),
              size=wx.Size(16, 13), style=0)
        self.infocheck.SetValue(False)
        self.infocheck.SetToolTipString('Terminal on/off')
        self.infocheck.Bind(wx.EVT_CHECKBOX, self.OnInfocheckCheckbox,
              id=wxID_WXMAININFOCHECK)

        self.typetext = wx.StaticText(id=wxID_WXMAINTYPETEXT, label='',
              name='typetext', parent=self.panel2, pos=wx.Point(115, 24),
              size=wx.Size(46, 13), style=wx.ST_NO_AUTORESIZE | wx.ALIGN_RIGHT)
        self.typetext.SetForegroundColour(wx.Colour(57, 187, 198))
        self.typetext.SetBackgroundColour(wx.Colour(85, 85, 85))
        self.typetext.Bind(wx.EVT_RIGHT_DOWN, self.OnPopup)

        self.statustext = wx.StaticText(id=wxID_WXMAINSTATUSTEXT, label='STOP',
              name='statustext', parent=self.panel2, pos=wx.Point(56, 24),
              size=wx.Size(29, 13), style=0)
        self.statustext.SetForegroundColour(wx.Colour(57, 187, 198))
        self.statustext.SetBackgroundColour(wx.Colour(85, 85, 85))
        self.statustext.Bind(wx.EVT_RIGHT_DOWN, self.OnPopup)

        self.staticBitmap8 = wx.StaticBitmap(bitmap=wx.Bitmap('images/volup.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXMAINSTATICBITMAP8,
              name='staticBitmap8', parent=self.panel1, pos=wx.Point(16, 16),
              size=wx.Size(20, 10), style=0)
        self.staticBitmap8.Bind(wx.EVT_LEFT_DOWN, self.OnVolumeUPStart)
        self.staticBitmap8.Bind(wx.EVT_LEFT_UP, self.OnVolumaUPStop)
        self.staticBitmap8.Bind(wx.EVT_RIGHT_DOWN, self.OnPopupMain)

        self.staticBitmap9 = wx.StaticBitmap(bitmap=wx.Bitmap('images/voldown.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXMAINSTATICBITMAP9,
              name='staticBitmap9', parent=self.panel1, pos=wx.Point(16, 40),
              size=wx.Size(20, 10), style=0)
        self.staticBitmap9.Bind(wx.EVT_LEFT_DOWN, self.OnVolumeDOWNStart)
        self.staticBitmap9.Bind(wx.EVT_LEFT_UP, self.OnVolumeDOWNStop)
        self.staticBitmap9.Bind(wx.EVT_RIGHT_DOWN, self.OnPopupMain)

        self.staticBitmap10 = wx.StaticBitmap(bitmap=wx.Bitmap('images/vol.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXMAINSTATICBITMAP10,
              name='staticBitmap10', parent=self.panel1, pos=wx.Point(6, 18),
              size=wx.Size(10, 30), style=0)
        self.staticBitmap10.Bind(wx.EVT_RIGHT_DOWN, self.OnPopupMain)

        self.audiotypetext = wx.StaticText(id=wxID_WXMAINAUDIOTYPETEXT,
              label='', name='audiotypetext', parent=self.panel2,
              pos=wx.Point(161, 24), size=wx.Size(63, 13),
              style=wx.ST_NO_AUTORESIZE | wx.ALIGN_LEFT)
        self.audiotypetext.SetForegroundColour(wx.Colour(57, 187, 198))
        self.audiotypetext.SetBackgroundColour(wx.Colour(85, 85, 85))
        self.audiotypetext.Bind(wx.EVT_RIGHT_DOWN, self.OnPopup)

        self.staticBitmap11 = wx.StaticBitmap(bitmap=wx.Bitmap('images/prev-small-gray.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXMAINSTATICBITMAP11,
              name='staticBitmap11', parent=self.panel1, pos=wx.Point(192, 72),
              size=wx.Size(30, 24), style=0)
        self.staticBitmap11.SetToolTipString('Prev.')
        self.staticBitmap11.Bind(wx.EVT_LEFT_UP, self.OnPrev)
        self.staticBitmap11.Bind(wx.EVT_RIGHT_DOWN, self.OnPopupMain)
        self.staticBitmap11.Bind(wx.EVT_LEFT_DOWN, self.OnBitmapLeftDown)

        self.staticBitmap12 = wx.StaticBitmap(bitmap=wx.Bitmap('images/next-small-gray.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXMAINSTATICBITMAP12,
              name='staticBitmap12', parent=self.panel1, pos=wx.Point(240, 72),
              size=wx.Size(30, 24), style=0)
        self.staticBitmap12.SetToolTipString('Next')
        self.staticBitmap12.Bind(wx.EVT_LEFT_UP, self.OnNext)
        self.staticBitmap12.Bind(wx.EVT_RIGHT_DOWN, self.OnPopupMain)
        self.staticBitmap12.Bind(wx.EVT_LEFT_DOWN, self.OnBitmapLeftDown)

        self.infotext = wx.TextCtrl(id=wxID_WXMAININFOTEXT, name='infotext',
              parent=self.panel1, pos=wx.Point(8, 120), size=wx.Size(363, 112),
              style=wx.TE_RICH2 | wx.VSCROLL | wx.HSCROLL | wx.TE_MULTILINE,
              value='')
        self.infotext.Bind(wx.EVT_SET_FOCUS, self.OnInfotextSetFocus)
        self.infotext.Bind(wx.EVT_TEXT_MAXLEN, self.OnInfotextTextMaxlen,
              id=wxID_WXMAININFOTEXT)

        self.staticBitmap1 = wx.StaticBitmap(bitmap=wx.Bitmap('images/eq.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXMAINSTATICBITMAP1,
              name='staticBitmap1', parent=self.panel1, pos=wx.Point(352, 8),
              size=wx.Size(16, 16), style=0)
        self.staticBitmap1.SetToolTipString('Equalizer')
        self.staticBitmap1.Bind(wx.EVT_LEFT_UP, self.OnEQ)

        self.staticBitmap13 = wx.StaticBitmap(bitmap=wx.Bitmap('images/file.png',
              wx.BITMAP_TYPE_PNG), id=wxID_WXMAINSTATICBITMAP13,
              name='staticBitmap13', parent=self.panel1, pos=wx.Point(352, 40),
              size=wx.Size(16, 16), style=0)
        self.staticBitmap13.Bind(wx.EVT_LEFT_UP, self.OnPlaylist)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.infotext.Hide()
        self.SetSize((387,145))
        self.SetPosition((wx.SystemSettings_GetMetric(wx.SYS_SCREEN_X)/2-200,wx.SystemSettings_GetMetric(wx.SYS_SCREEN_Y)-200))
        wx.EVT_END_PROCESS(self, -1, self.OnProcessEnded)
        self.move=False;
        self.xpos=0
        self.ypos=0
        self.fname=''
        self.subname=''
        self.fnamepos=0
        self.irany=0
        self.process=None
        self.pid=0
        self.estream=None
        self.istream=None
        self.ostream=None
        self.prev_vlm=0
        self.maxtime=0
        self.video_opt=''
        self.audio_opt=''
        self.osd_opt=''
        self.dvd_opt=''
        self.adv_cmd=''
        self.dvd_device=''
        self.dvd_prefered_alang='en'
        self.dvd_prefered_slang=''
        self.vol_dir=0
        self.winlirc_enable=False
        self.winlirc=None
        self.winlirc_play=''
        self.winlirc_stop=''
        self.winlirc_pause=''
        self.winlirc_mute=''
        self.winlirc_vol_up=''
        self.winlirc_vol_down=''
        self.winlirc_seek_p=''
        self.winlirc_seek_m=''
        self.winlirc_osd=''
        self.play_type=PLAY_LIST
        self.log=''
        self.error=''
        self.remaing_time=False
        self.loadcfg()
        self.loadhistory()
        self.panel2.SetFocus()
        self.DragAcceptFiles(True)
        self.DisableMediaMenu()
        self.cur_item=0
        self.currentsec = 0.0
        self.lastpath=''

        try:
            winh=win32ui.GetActiveWindow()
            winh.HookMessage(self.msgh,win32con.WM_COPYDATA)
        except Exception, e:
            print e
            print 'HIBA!\n'

        self.plwin=PListFrame.create(self)

        if self.winlirc_enable:
            self.winlirc=winlirc.winlirc_client()
            if not self.winlirc.connected:
                del self.winlirc
                self.winlirc=None
                dlg = wx.MessageDialog(self, 'Can not connect to WinLirc Server!\nIf you want to use WinLirc please start the WinLirc, and restart MPlayerC.',
                      'Error', wx.OK | wx.ICON_ERROR)
                try:
                    dlg.ShowModal()
                finally:
                    dlg.Destroy()
            else:
                self.timer3.Start(50)

        self.timer1.Start(300)

    def OnWxMainClose(self, event):
        self.timer1.Stop()
        self.OnStop(None)
        self.savehistory()
        if self.winlirc != None and self.winlirc.thrd != None:
            self.winlirc.thrd.join(0.2)
        if event != None:
            event.Skip()

        self.plwin.savePlayList()

    def OnMute(self, event):
        if self.ostream != None and self.statustext.GetLabel() == 'PLAY':
            self.ostream.write("mute\n")
            if self.staticBitmap4.GetToolTip().GetTip() == 'Mute on':
                self.ostream.write("osd_show_text MUTE=ON\n")
                self.staticBitmap4.SetToolTipString('Mute off')
            else:
                self.ostream.write("osd_show_text MUTE=OFF\n")
                self.staticBitmap4.SetToolTipString('Mute on')
        self.staticBitmap4.SetBitmap(wx.Bitmap('images/mute-small.png',wx.BITMAP_TYPE_PNG))
        if event != None:
            event.Skip()

    def OnOpenFile(self, event):
        self.staticBitmap3.SetBitmap(wx.Bitmap('images/folder-small.png',wx.BITMAP_TYPE_PNG))
        if self.process == None:
            dlg=FilesDialog.create(self)
            try:
                if (self.lastpath):
                    dlg.genericDirCtrl1.SetPath(self.lastpath)
                if dlg.ShowModal() == wx.ID_OK:
                    self.timer1.Stop()
                    self.fname='"'+dlg.videofile_path.GetLabel()+'"'
                    self.subname='"'+dlg.subfile_path.GetLabel()+'"'
                    self.filename.SetLabel(self.fname[:44])
                    self.fnamepos=0
                    self.irany=1
                    self.lastpath=dlg.genericDirCtrl1.GetPath()
                    self.plwin.listCtrl1.DeleteAllItems()
                    self.play_type=PLAY_LIST
                    self.plwin.pl_type.SetSelection(0)
                    r,e=os.path.splitext(self.fname)
                    if e.lower()=='.pls"' or e.lower()=='.m3u"':
                        self.ParseM3U(self.fname)
                    else:
                        self.plwin.appendItem(self.fname.strip('"'), self.subname.strip('"'))
                    self.timer1.Start(300)

            finally:
                dlg.Destroy()
        event.Skip()

    def OnWxMainSize(self, event):
        if self.infocheck.IsChecked() == True:
            self.SetSize((386,270))
        else:
            self.SetSize((386,145))
        event.Skip()

    def OnTimer1Timer(self, event):
        if len(self.fname) >44:
            if self.fnamepos+44  >= len(self.fname) :
                self.irany = -1
            elif self.fnamepos  <= 0 :
                self.irany = 1

            self.fnamepos += self.irany
            self.filename.SetLabel(self.fname[self.fnamepos:self.fnamepos+44])

        self.readIStreams()

        if event != None:
            event.Skip()

    def readIStreams(self):
        try:
            if self.istream != None and self.process != None:
                if self.istream.Eof() != True:

                    while self.istream.CanRead() == True:
                        self.log=self.istream.read()
                        if len(self.log) > 0:
                            self.log=self.log.replace("\r","\n")
                            self.log=self.log.replace("\b","")
                            while self.log.find('\n') > -1:
                                info=self.log[:self.log.find('\n')]
                                self.log=self.log[self.log.find('\n')+1:]
                                if info :
    ##                    while self.istream.CanRead() == True:
    ##                        info=self.istream.readline()
                                    if info.find('A:') == 0 or info.find('V:') ==0:
                                        if self.statustext.GetLabel() != 'PLAY' :
                                            self.statustext.SetLabel('PLAY')
                                        infolist=info[2:].split()
                                        if len(infolist) > 1 and infolist[0].isspace() ==False:
                                            if infolist[0].find(':') == -1:
                                                sec=float(infolist[0])
                                            else:
                                                sec=0
                                                x=infolist[0].split(':')
                                                x.reverse()
                                                for i in range(len(x)):
                                                    sec+=float(x[i])*pow(60,i)
                                            if self.remaing_time:
                                                min='- %02d:%02d:%02d' % ((self.maxtime-sec)/3600,((self.maxtime-sec)%3600)/60,(self.maxtime-sec)%60)
                                            else:
                                                min='  %02d:%02d:%02d' % (sec/3600,(sec%3600)/60,sec%60)
                                            self.timepos.SetLabel(min)
                                            if self.maxtime > 0 :
                                                self.gauge1.SetValue(int(sec/self.maxtime*100))
                                                self.gauge1.SetToolTipString(str(self.gauge1.GetValue())+'%')
                                            self.currentsec = sec
                                    elif info.find('ID_LENGTH=') == 0:
                                        info=info.strip("\n")
                                        self.maxtime=int(float(info[10:]))
                                    elif info.find('ANS_LENGTH=') == 0:
                                        info=info.strip("\n")
                                        self.maxtime=int(info[11:])
                                    elif info.find('ID_VIDEO_FORMAT=') == 0:
                                        info=info.strip("\n")
                                        self.typetext.SetLabel(info[16:].upper())
                                    elif info.find('ID_AUDIO_CODEC=') == 0:
                                        info=info.strip("\n")
                                        label=self.typetext.GetLabel()
                                        if len(label)>0:
                                            self.audiotypetext.SetLabel("/"+info[15:].upper())
                                        else:
                                            self.audiotypetext.SetLabel(info[15:].upper())
                                    elif info.find('PAUSE') > -1:
                                        self.statustext.SetLabel('PAUSE')
                                    elif info.find('Cache fill:') == 0:
                                        if self.statustext.GetLabel() != 'CACHEING':
                                            self.statustext.SetLabel('CACHEING')
                                            self.statustext.OnPaint()
                                    elif self.play_type==PLAY_URL and info.find('Connecting'):
                                        self.statustext.SetLabel('CONNECTING')
                                    else:
                                        self.infotext.AppendText(info+'\n')

            if self.estream != None and self.process != None:
                if self.estream.Eof() != True:

                    while self.estream.CanRead() == True:
                        self.error+=self.estream.read()
                        if len(self.error) > 0:
                            self.error=self.error.replace("\r","\n")
                            self.error=self.error.replace("\b","")
                            while self.error.find('\n') > -1:
                                error=self.error[:self.error.find('\n')]
                                self.error=self.error[self.error.find('\n')+1:]
                                if error:
                                    self.infotext.SetDefaultStyle(wx.TextAttr(wx.RED))
                                    self.infotext.AppendText(error+'\n')
                                    self.infotext.SetDefaultStyle(wx.TextAttr(wx.BLACK))
        except Exception, e:
            print e
            pass

    def OnSettings(self, event):
        self.staticBitmap2.SetBitmap(wx.Bitmap('images/setting-small.png',wx.BITMAP_TYPE_PNG))
        event.Skip()
        setdlg=Settings.settingsdlg(self)
        setdlg.ShowModal()
        self.loadcfg()

    def OnPlay(self, event):
        self.staticBitmap5.SetBitmap(wx.Bitmap('images/play-small.png',wx.BITMAP_TYPE_PNG))

        if self.process != None:
            return

        self.infotext.Clear()
        if ((not os.path.isfile(self.fname.strip('"')))\
         and self.play_type == PLAY_FILE) or\
         (self.plwin.listCtrl1.GetItemCount()==0 and self.play_type == PLAY_LIST):
            self.infotext.AppendText('No file!\n')
            dlg = wx.MessageDialog(self, 'Input file not found! Please, open a file.',
              'Info', wx.OK | wx.ICON_INFORMATION)
            try:
                dlg.ShowModal()
            finally:
                dlg.Destroy()
            return

        self.process = wx.Process(self)
        try:
            self.process.Redirect()

            if self.play_type ==PLAY_FILE:
                self.addhistory(self.fname)
                cmdline=EXEPATH + " -slave -identify "+self.video_opt+\
                    self.audio_opt+self.osd_opt+self.adv_cmd+" "+self.fname+" "
                if os.path.isfile(self.subname.strip('"')):
                    cmdline+=" -sub "+self.subname

            elif self.play_type == PLAY_DVD:
                cmdline=EXEPATH + " -slave -identify "+self.video_opt+\
                    self.audio_opt+self.osd_opt+self.adv_cmd+" "+self.dvd_device+" "+self.dvd_opt+" "

            elif self.play_type == PLAY_LIST:
                plist_type=self.plwin.pl_type.GetStringSelection()
                if plist_type=="Normal":
                    if self.cur_item >= self.plwin.listCtrl1.GetItemCount() or self.cur_item < 0:
                        self.process.Detach()
                        self.process=None
                        return
                    self.fname='"'+self.plwin.listCtrl1.GetItem(self.cur_item,1).GetText()+'"'
                    self.subname='"'+self.plwin.listCtrl1.GetItem(self.cur_item,2).GetText()+'"'
                    self.plwin.listCtrl1.SetItemImage(self.cur_item,1,1)
                elif plist_type=="Repeat All":
                    if self.cur_item >= self.plwin.listCtrl1.GetItemCount() :
                        self.cur_item=0  
                    if self.cur_item < 0:
                        self.cur_item=self.plwin.listCtrl1.GetItemCount()-1
                    self.fname='"'+self.plwin.listCtrl1.GetItem(self.cur_item,1).GetText()+'"'
                    self.subname='"'+self.plwin.listCtrl1.GetItem(self.cur_item,2).GetText()+'"'
                    self.plwin.listCtrl1.SetItemImage(self.cur_item,1,1)
                elif plist_type=="Repeat One":
                    self.fname='"'+self.plwin.listCtrl1.GetItem(self.cur_item,1).GetText()+'"'
                    self.subname='"'+self.plwin.listCtrl1.GetItem(self.cur_item,2).GetText()+'"'
                    self.plwin.listCtrl1.SetItemImage(self.cur_item,1,1)
                elif plist_type=="Random":
                    import random
                    self.cur_item=random.randint(0,self.plwin.listCtrl1.GetItemCount()-1)
                    self.fname='"'+self.plwin.listCtrl1.GetItem(self.cur_item,1).GetText()+'"'
                    self.subname='"'+self.plwin.listCtrl1.GetItem(self.cur_item,2).GetText()+'"'
                    self.plwin.listCtrl1.SetItemImage(self.cur_item,1,1)

                self.plwin.move_up_btn.Enable(False)
                self.plwin.move_down_btn.Enable(False)
                self.plwin.del_btn.Enable(False)

                self.filename.SetLabel(self.fname[:44])
                self.fnamepos=0
                self.irany=1
                self.addhistory(self.fname)
                cmdline=EXEPATH + " -slave -identify "+self.video_opt+\
                    self.audio_opt+self.osd_opt+self.adv_cmd+" "+self.fname+" "
                if os.path.isfile(self.subname.strip('"')):
                    cmdline+=" -sub "+self.subname
            elif self.play_type == PLAY_URL:
                self.addhistory(self.fname)
                cmdline=EXEPATH + " -slave -identify "+self.video_opt+\
                    self.audio_opt+self.osd_opt+self.adv_cmd+" "+self.fname+" "
                
            self.currentsec -= 5.0
            if self.currentsec>0: 
                cmdline += ' -ss %s'%str(self.currentsec)

            self.infotext.AppendText(cmdline+'\n')
            self.pid = wx.Execute(cmdline, wx.EXEC_ASYNC, self.process)
            self.estream=self.process.GetErrorStream()
            self.istream=self.process.GetInputStream()
            self.ostream=self.process.GetOutputStream()
            self.statustext.SetLabel('PLAY')
            self.ostream.write("osd_show_text PLAY\n")
            self.ostream.write("volume 1\n")
            self.EnbleMediaMenu()
            self.DisableFileMenu()
            try:
                ctypes.windll.user32.SystemParametersInfoA(17,False,0,0)
            except Exception, e:
                print e
                pass
        except Exception, e:
            print e
            if self.process is not None:
                self.process.Detach()
                self.process=None
            self.istream=None
            self.estream=None
            self.ostream=None
            self.statustext.SetLabel('ERROR')

        if event != None:
            event.Skip()

    def OnStop(self, event):
        if event is not None:
            self.plwin.refreshTime()
        self.currentsec = 0.0

        self.staticBitmap6.SetBitmap(wx.Bitmap('images/stop-small.png',wx.BITMAP_TYPE_PNG))
        if self.pid == 0 or self.process == None:
            return
        try:
            if self.ostream != None:
                self.ostream.write("osd_show_text STOP\n")
                self.ostream.write("quit\n")
                self.timer2.Stop()
                time.sleep(1)
            if self.process != None:
                winver=sys.getwindowsversion()
                if winver[0]<5 or (winver[0]==5 and winver[1]==0):## no XP :(
                    if wxProcess_Exists(self.pid):
                        wxProcess_Kill(self.pid,wxSIGKILL)
                self.statustext.SetLabel('STOP')
                self.process.CloseOutput()
                self.process.Detach()
                self.istream=None
                self.process=None
                self.ostream=None
                self.process=None
            self.gauge1.SetValue(0)
            self.gauge1.SetToolTipString('0%')
            self.timepos.SetLabel('  00:00:00')
            self.typetext.SetLabel('')
            self.audiotypetext.SetLabel('')
            self.maxtime=0
            if self.play_type==PLAY_LIST:
                self.plwin.listCtrl1.SetItemImage(self.cur_item,0,0)
                self.plwin.move_up_btn.Enable(True)
                self.plwin.move_down_btn.Enable(True)
                self.plwin.del_btn.Enable(True)
            self.cur_item=0
            self.DisableMediaMenu()
            self.EnableFileMenu()
        except Exception, e:
            print e
            self.statustext.SetLabel('ERROR')
        try:
            ctypes.windll.user32.SystemParametersInfoA(17,True,0,0)
        except Exception, e:
            print e
            pass
        if event !=None :
            event.Skip()
            
            
    def OnPause(self, event):
        if self.ostream != None:
            if self.statustext.GetLabel() == 'PLAY':
                self.ostream.write("osd_show_text PAUSE\n")
                self.ostream.write("pause\n")
            else:
                self.ostream.write("osd_show_text PLAY\n")
                pass
        self.staticBitmap7.SetBitmap(wx.Bitmap('images/pause-small.png',wx.BITMAP_TYPE_PNG))
        if event != None:
            event.Skip()

    def OnProcessEnded(self, event):
        if self.process is not None:
            self.readIStreams()
            self.process.CloseOutput()
            self.process.Detach()
            self.process=None
            self.istream=None
            self.process=None
            self.ostream=None

        self.gauge1.SetValue(0)
        self.gauge1.SetToolTipString('0%')
        self.timepos.SetLabel('  00:00:00')
        self.typetext.SetLabel('')
        self.audiotypetext.SetLabel('')
        self.maxtime=0

        self.DisableMediaMenu()
        self.EnableFileMenu()

        try:
            ctypes.windll.user32.SystemParametersInfoA(17,True,0,0)
        except Exception, e:
            print e
            pass

        ec=event.GetExitCode()
        if ec != 0:
            self.statustext.SetLabel('ERROR')
        else:
            self.statustext.SetLabel("STOP")

        if self.play_type==PLAY_LIST:
            self.currentsec = 0.0
            self.plwin.refreshTime()
            self.plwin.listCtrl1.SetItemImage(self.cur_item,0,0)
            if self.plwin.pl_type.GetStringSelection()=="Normal" or self.plwin.pl_type.GetStringSelection()=="Repeat All":
                self.cur_item=self.cur_item+1
            if (self.cur_item < self.plwin.listCtrl1.GetItemCount()\
             and self.plwin.pl_type.GetStringSelection()=="Normal")\
             or self.plwin.pl_type.GetStringSelection()<>"Normal":
                self.OnPlay(None)
            else:
                self.cur_item=0
                self.plwin.move_up_btn.Enable(True)
                self.plwin.move_down_btn.Enable(True)
                self.plwin.del_btn.Enable(True)
        event.Skip()

    def OnInfocheckCheckbox(self, event):
        if self.infocheck.IsChecked() == True:
            self.SetSize((386,270))
            self.infotext.Show()
        else:
            self.SetSize((386,145))
            self.infotext.Hide()
        event.Skip()
        self.panel2.SetFocus()

    def OnGauge1LeftDown(self, event):
        if self.ostream != None:
            (w,h)=self.gauge1.GetSizeTuple()
            value=(float(event.m_x+2)/w)*self.maxtime
            self.ostream.write("seek "+str(int(value))+" 2\n")
        event.Skip()

    def loadcfg(self):
        opt=[]
        cfgfile=None
        try:
            cfgfile=open('mplayerc.cfg','r')
            line=cfgfile.readline()
            while(len(line) > 0) :
                line=line.rstrip('\n')
                line=line.rstrip('\r')
                if len(line) > 0 and line.find('=') > -1:
                    ##opt.append(tuple(line.split('=')))
                    opt.append((line[0:line.find('=')],line[line.find('=')+1:]))
                line=cfgfile.readline()
            cfgfile.close()
            options=dict(opt)
            self.video_opt=' '
            self.audio_opt=' '
            self.osd_opt=' '
            #Video options
            if options.has_key('vo'):
                self.video_opt+=' -vo '+str(options['vo'])+' '
            if options.has_key('double'):
                if (Boolean(options['double'])) == True:
                    self.video_opt+=' -double '
            if options.has_key('dr'):
                if (Boolean(options['dr'])) == True:
                    self.video_opt+=' -dr '
            if options.has_key('framedrop'):
                if (Boolean(options['framedrop'])) == True:
                    self.video_opt+=' -framedrop '
            if options.has_key('idx'):
                if (Boolean(options['idx'])) == True:
                    self.video_opt+=' -idx '
            if options.has_key('panscan') or options.has_key('postp'):
                if (Boolean(options['panscan'])) == True or (Boolean(options['postp'])) == True:
                    self.video_opt+=' -vf '
            if options.has_key('panscan'):
                if (Boolean(options['panscan'])) == True:
                    if options.has_key('panscanvalue'):
                        self.video_opt+='crop='+options['panscanvalue']
            if options.has_key('postp'):
                if (Boolean(options['postp'])) == True:
                    if self.video_opt[-3:-1] == 'vf':
                        self.video_opt+=' pp -autoq 10 '
                    else:
                        self.video_opt+=',pp -autoq 10 '
            if options.has_key('usecache'):
                if (Boolean(options['usecache'])) == True:
                    if options.has_key('cachesize'):
                        self.video_opt+=' -cache '+options['cachesize']+' '

            #Audio options
            ##self.audio_opt=' -softvol -softvol-max 200 -af '
            self.audio_opt=' -softvol -af '
            if options.has_key('normvol'):
                if (Boolean(options['normvol'])) == True:
                    self.audio_opt+='volnorm,'
            if options.has_key('defvol'):
                self.audio_opt+='volume='+options['defvol']+':0 '
            if options.has_key('channels'):
                if options['channels'] =='4-speakers':
                    self.audio_opt+=' -channels 4'
                elif options['channels'] =='5.1-speakers':
                    self.audio_opt+=' -channels 6'
            if options.has_key('audiodelay'):
                if int(options['audiodelay']) != 0:
                    self.audio_opt+=' -delay '+\
                        str(float(options['audiodelay'])/10)+' '
            #OSD options
            if options.has_key('fontpath'):
                self.osd_opt+=' -font "'+options['fontpath']+'" '
            if options.has_key('overlapping'):
                if (Boolean(options['overlapping'])) == True:
                    self.osd_opt+=' -overlapsub '
            if options.has_key('osdpos'):
                if options['osdpos'] == 'Top':
                    self.osd_opt+=' -subpos 0'
                elif options['osdpos'] == 'Center':
                    self.osd_opt+=' -subpos 50'
            if options.has_key('osdbgalpha'):
                self.osd_opt+=' -sub-bg-alpha '+options['osdbgalpha']
            if options.has_key('osdbggray'):
                self.osd_opt+=' -sub-bg-color '+options['osdbggray']
            if options.has_key('osdblackband'):
                if (Boolean(options['osdblackband'])) == True:
                    self.osd_opt+=' -vf expand=0:-50:0:0 '
            #WinLirc
            if options.has_key('winlirc') and Boolean(options['winlirc'])==True:
                self.winlirc_enable=True
                if options.has_key('winlirc_play'):
                    self.winlirc_play=options['winlirc_play']
                if options.has_key('winlirc_stop'):
                    self.winlirc_stop=options['winlirc_stop']
                if options.has_key('winlirc_pause'):
                    self.winlirc_pause=options['winlirc_pause']
                if options.has_key('winlirc_mute'):
                    self.winlirc_mute=options['winlirc_mute']
                if options.has_key('winlirc_vol_up'):
                    self.winlirc_vol_up=options['winlirc_vol_up']
                if options.has_key('winlirc_vol_down'):
                    self.winlirc_vol_down=options['winlirc_vol_down']
                if options.has_key('winlirc_seek_+'):
                    self.winlirc_seek_p=options['winlirc_seek_+']
                if options.has_key('winlirc_seek_-'):
                    self.winlirc_seek_m=options['winlirc_seek_-']
                if options.has_key('winlirc_fs'):
                    self.winlirc_fs=options['winlirc_fs']
                if options.has_key('winlirc_exit'):
                    self.winlirc_exit=options['winlirc_exit']
                if options.has_key('winlirc_osd'):
                    self.winlirc_osd=options['winlirc_osd']
            #DVD
            if options.has_key('dvd_alang'):
                self.dvd_prefered_alang=options['dvd_alang']
            if options.has_key('dvd_slang'):
                self.dvd_prefered_slang=options['dvd_slang']
            #Advanced cmd.line
            self.adv_cmd=''
            if options.has_key('adv_cmd'):
                self.adv_cmd=' '+options['adv_cmd']+' '

        except Exception, e:
            print e
            if cfgfile!=None :
                cfgfile.close()
            dlg = wx.MessageDialog(self, 'Error, when reading/parsing config file.',
              'Config Error', wx.OK | wx.ICON_ERROR)
            try:
                dlg.ShowModal()
            finally:
                dlg.Destroy()

    def OnVolumeUPStart(self, event):
        if self.statustext.GetLabel() == 'PLAY':
            if self.ostream != None:
                self.ostream.write("volume 1\n")
            self.vol_dir=1
            self.timer2.Start(100)
        self.staticBitmap8.SetBitmap(wx.Bitmap('images/volup-down.png',wx.BITMAP_TYPE_PNG))
        event.Skip()

    def OnVolumeDOWNStart(self, event):
        if self.statustext.GetLabel() == 'PLAY':
            if self.ostream != None:
                self.ostream.write("volume -1\n")
            self.vol_dir=0
            self.timer2.Start(100)
        self.staticBitmap9.SetBitmap(wx.Bitmap('images/voldown-down.png',wx.BITMAP_TYPE_PNG))
        event.Skip()

    def OnVolumaUPStop(self, event):
        self.timer2.Stop()
        self.staticBitmap8.SetBitmap(wx.Bitmap('images/volup.png',wx.BITMAP_TYPE_PNG))
        event.Skip()

    def OnVolumeDOWNStop(self, event):
        self.timer2.Stop()
        self.staticBitmap9.SetBitmap(wx.Bitmap('images/voldown.png',wx.BITMAP_TYPE_PNG))
        event.Skip()

    def OnTimer2Timer(self, event):
        if self.vol_dir==0:
            if self.ostream != None:
                self.ostream.write("volume -1\n")
        else:
            if self.ostream != None:
                self.ostream.write("volume 1\n")
        event.Skip()

    def OnTimer3Timer(self, event):
        try:
            if self.winlirc_enable and self.winlirc != None and self.winlirc.connected:
                (rep_num,winlirc_cmd)=self.winlirc.getButton()
                if rep_num != -1:
                    if winlirc_cmd==self.winlirc_play and rep_num == 0:
                        self.OnPlay(None)
                    elif winlirc_cmd==self.winlirc_stop and rep_num == 0:
                        self.OnStop(None)
                    elif winlirc_cmd==self.winlirc_pause and rep_num == 0:
                        self.OnPause(None)
                    elif winlirc_cmd==self.winlirc_mute and rep_num == 0:
                        self.OnMute(None)
                    elif winlirc_cmd==self.winlirc_vol_up:
                        if self.ostream != None:
                            self.ostream.write("volume 1\n")
                    elif winlirc_cmd==self.winlirc_vol_down:
                        if self.ostream != None:
                            self.ostream.write("volume -1\n")
                    elif winlirc_cmd==self.winlirc_seek_p:
                        if self.ostream != None:
                            self.ostream.write("seek 60\n")
                    elif winlirc_cmd==self.winlirc_seek_m:
                        if self.ostream != None:
                            self.ostream.write("seek -60\n")
                    elif winlirc_cmd==self.winlirc_fs and rep_num == 0:
                        if self.ostream != None:
                            self.ostream.write("vo_fullscreen\n")
                    elif winlirc_cmd==self.winlirc_exit and rep_num == 0:
                        self.OnClose(None)
                    elif winlirc_cmd==self.winlirc_osd and rep_num == 0:
                        if self.ostream != None:
                            self.ostream.write("osd\n")
        except Exception, e:
            print e
            pass

        event.Skip()

    def OnPopupMain(self, event):
        obj=event.GetEventObject()
        if obj != self.panel1:
            (x,y)=obj.GetPositionTuple()
        else:
            (x,y)=(0,0)
        self.PopupMenuXY(self.popupmenu,event.m_x+x,event.m_y+y)
        event.Skip()

    def OnPopup(self, event):
        obj=event.GetEventObject()
        if obj != self.panel2:
            (x,y)=obj.GetPositionTuple()
        else:
            (x,y)=(0,0)
        (px,py)=self.panel2.GetPositionTuple()
        self.PopupMenuXY(self.popupmenu,event.m_x+x+px,event.m_y+y+py)
        event.Skip()

    def OnFullscreen(self, event):
        if self.ostream != None and self.process != None:
            self.ostream.write("vo_fullscreen\n")
        if event != None:
            event.Skip()

    def OnPanel2KeyDown(self, event):
        key=event.GetKeyCode()
        if key==32 or key==179:
            if self.statustext.GetLabel()=='STOP':
                self.OnPlay(None)
            else:
                self.OnPause(None)
        elif key==70:
            self.OnFullscreen(None)
        elif key==81 or key==178:
            self.OnStop(None)
        elif key==48:
            if self.ostream != None:
                self.ostream.write("volume 1\n")
        elif key==57:
            if self.ostream != None:
                self.ostream.write("volume -1\n")
        elif key==wx.WXK_LEFT or key==177:
            if self.ostream != None:
                self.ostream.write("seek -10\n")
        elif key==wx.WXK_RIGHT or key==176:
            if self.ostream != None:
                self.ostream.write("seek 10\n")
        elif key==wx.WXK_UP:
            if self.ostream != None:
                self.ostream.write("seek 60\n")
        elif key==wx.WXK_DOWN:
            if self.ostream != None:
                self.ostream.write("seek -60\n")
        elif key==77:
            self.OnMute(None)
        elif key==79:
            if self.ostream != None:
                self.ostream.write("osd\n")
        elif key==74 and event.ControlDown()==True:
            if self.statustext.GetLabel()=='PLAY':
                dlg = wx.TextEntryDialog(self, 'Set a seeking time in minutes.', 'Seek', '')
                try:
                    if dlg.ShowModal() == wx.ID_OK:
                        answer = dlg.GetValue()
                        try:
                            time=int(answer)*60
                            if self.ostream != None:
                                self.ostream.write("seek "+str(time)+" 2\n")
                        except Exception, e:
                            print e
                            self.infotext.SetDefaultStyle(wx.TextAttr(wx.RED))
                            self.infotext.AppendText('Error when try seeking!\n')
                            self.infotext.SetDefaultStyle(wx.TextAttr(wx.BLACK))
                finally:
                    dlg.Destroy()
        elif key==49:
            if self.ostream != None:
                self.ostream.write("brightness -5\n")
        elif key==50:
            if self.ostream != None:
                self.ostream.write("brightness +5\n")
        elif key==51:
            if self.ostream != None:
                self.ostream.write("contrast -5\n")
        elif key==52:
            if self.ostream != None:
                self.ostream.write("contrast +5\n")
        elif key==53:
            if self.ostream != None:
                self.ostream.write("hue -5\n")
        elif key==54:
            if self.ostream != None:
                self.ostream.write("hue +5\n")
        else :
            self.ostream.write("key_down_event %d\n" % key)
        event.Skip()

    def OnInfotextSetFocus(self, event):
        self.panel2.SetFocus()
        event.Skip()

    def OnAbout(self, event):
        dlg=AboutDialog.Aboutdlg(self)
        try:
            dlg.ShowModal()
        finally:
            dlg.Destroy()
        event.Skip()

    def OnHelp(self, event):
        file="file://"+os.getcwd()+"\\help\\mplayerc-help.html"
        win32api.ShellExecute(0,"open",file,"","",0)
        event.Skip()


    def addhistory(self,fname):
        if self.historymenu.FindItem(fname) == wx.NOT_FOUND:
            count = self.historymenu.GetMenuItemCount()
            if count>10:
                id = self.historymenu.FindItemByPosition(3).GetId()
                self.historymenu.DestroyId(id)
            id= wx.NewId()
            self.historymenu.Prepend(id,fname)
            self.Bind(wx.EVT_MENU, self.OnLoadHistory, id=id)

    def OnLoadHistory(self,event):
        if self.process == None:
            self.fnamepos=0
            self.irany=1
            self.fname=self.historymenu.GetLabel(event.GetId())
            self.filename.SetLabel(self.fname)
            r,e=os.path.splitext(self.fname)
            self.play_type=PLAY_LIST
            self.plwin.listCtrl1.DeleteAllItems()
            if e.lower()=='.pls"' or e=='.m3u"':
                self.ParseM3U(self.fname)
            else:
                self.plwin.appendItem(self.fname.strip('"'), self.subname.strip('"'))
        event.Skip()

    def loadhistory(self):
        try:
            import codecs
            hf=codecs.open('history.lst','r',encoding='utf-8')
        except Exception, e:
            print e
            return
        try:
            hlines=hf.readlines()
            for line in hlines:
                line=line.strip('\n')
                self.addhistory(line)
        finally:
            hf.close()

    def savehistory(self):
        try:
            import codecs
            hf=codecs.open('history.lst','w', encoding='utf-8')
        except Exception, e:
            print e
            return

        try:
            hl=self.historymenu.GetMenuItems()
            for poz in range(len(hl)-1,-1,-1):
                if hl[poz].GetText() !='Clear' and len(hl[poz].GetText()) > 0:
                    hf.write(hl[poz].GetText()+'\n')


        finally:
            hf.close()

    def OnHistoryClear(self, event):
        self.popupmenu.DestroyId(wxID_WXMAINPOPUPMENUITEMS12)
        self.historymenu = wx.Menu(title='')
        self._init_coll_historymenu_Items(self.historymenu)
        self.popupmenu.InsertMenu(4,wxID_WXMAINPOPUPMENUITEMS12,'History',self.historymenu,'')
        try:
            import codecs
            hf=codecs.open('history.lst','w',encoding='utf-8')
        except Exception, e:
            print e
            return
        hf.close()
        event.Skip()

    def OnOpenDVD(self, event):
        if self.process == None:
            dvddlg=DVD_dialog.create(self)
            dvddlg.prefered_alang=self.dvd_prefered_alang
            dvddlg.prefered_slang=self.dvd_prefered_slang
            try:
                if dvddlg.ShowModal()==wx.ID_OK:
                    self.dvd_device=' -dvd-device "'+dvddlg.dvd_dev+'" '
                    self.dvd_opt='dvd://'+str(dvddlg.titlenum.GetSelection()+1)+' '
                    if dvddlg.alang.GetStringSelection():
                        self.dvd_opt+=' -alang '+dvddlg.alang.GetStringSelection()+' '
                    if dvddlg.slang.GetStringSelection():
                        self.dvd_opt+=' -slang '+dvddlg.slang.GetStringSelection()+' '
                    self.fnamepos=0
                    self.irany=1
                    self.fname='DVD ON:"'+dvddlg.dvd_dev+\
                        '" '+dvddlg.titlenum.GetStringSelection()
                    self.filename.SetLabel(self.fname)
                    self.play_type=PLAY_DVD
                    self.plwin.listCtrl1.DeleteAllItems()

            finally:
                dvddlg.Destroy()

        event.Skip()

    def OnNext(self, event):
        if self.play_type==PLAY_LIST :
            item=self.cur_item
            self.OnStop(None)
            self.cur_item=item+1
            self.OnPlay(None)
        elif self.play_type==PLAY_DVD :
            if self.ostream != None:
                self.ostream.write("seek_chapter 1 0\n")    
        self.staticBitmap12.SetBitmap(wx.Bitmap('images/next-small-gray.png',wx.BITMAP_TYPE_PNG))
        event.Skip()

    def OnPrev(self, event):
        if self.play_type==PLAY_LIST :
            item=self.cur_item
            if item > 0:
                self.OnStop(None)
                self.cur_item=item-1
                self.OnPlay(None)
        elif self.play_type==PLAY_DVD :
            if self.ostream != None:
                self.ostream.write("seek_chapter -1 0\n")    
        self.staticBitmap11.SetBitmap(wx.Bitmap('images/prev-small-gray.png',wx.BITMAP_TYPE_PNG))
        event.Skip()

    def OnInfotextTextMaxlen(self, event):
        self.infotext.Clear()
        event.Skip()

    def OnBitmapLeftDown(self, event):
        bitmap=event.GetEventObject()
        if bitmap==self.staticBitmap7 :
            bitmap.SetBitmap(wx.Bitmap('images/pause-small-down.png',wx.BITMAP_TYPE_PNG))
        elif bitmap==self.staticBitmap4 :
            bitmap.SetBitmap(wx.Bitmap('images/mute-small-down.png',wx.BITMAP_TYPE_PNG))
        elif bitmap==self.staticBitmap6 :
            bitmap.SetBitmap(wx.Bitmap('images/stop-small-down.png',wx.BITMAP_TYPE_PNG))
        elif bitmap==self.staticBitmap5 :
            bitmap.SetBitmap(wx.Bitmap('images/play-small-down.png',wx.BITMAP_TYPE_PNG))
        elif bitmap==self.staticBitmap11 :
            bitmap.SetBitmap(wx.Bitmap('images/prev-small-gray-down.png',wx.BITMAP_TYPE_PNG))
        elif bitmap==self.staticBitmap12 :
            bitmap.SetBitmap(wx.Bitmap('images/next-small-gray-down.png',wx.BITMAP_TYPE_PNG))
        elif bitmap==self.staticBitmap3 :
            bitmap.SetBitmap(wx.Bitmap('images/folder-small-down.png',wx.BITMAP_TYPE_PNG))
        elif bitmap==self.staticBitmap2 :
            bitmap.SetBitmap(wx.Bitmap('images/setting-small-down.png',wx.BITMAP_TYPE_PNG))
        elif bitmap==self.staticBitmap2 :
            bitmap.SetBitmap(wx.Bitmap('images/setting-small-down.png',wx.BITMAP_TYPE_PNG))
        event.Skip()

    def OnClose(self, event):
        self.Close()
        event.Skip()

    def DisableMediaMenu(self):
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS7, False)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS6, False)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS17, False)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS18, False)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS13, False)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS8, False)

    def EnbleMediaMenu(self):
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS7, True)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS6, True)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS17, True)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS18, True)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS13, True)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS8, True)

    def DisableFileMenu(self):
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS10, False)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS12, False)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS15, False)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS16, False)

    def EnableFileMenu(self):
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS10, True)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS12, True)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS15, True)
        self.popupmenu.Enable(wxID_WXMAINPOPUPMENUITEMS16, True)

    def OnWxMainDropFiles(self, event):
        self.infotext.AppendText('***** Dropped:'+str(len(event.GetFiles()))+'file. ****\n')
        self.infotext.AppendText('***** Stop playing!****\n')
        if self.process != None:
            self.OnStop(None)
        ##self.infotext.AppendText('***** Restart playing!****\n')
        ##self.fname='"'+event.GetFiles()[0]+'"'
        ## self.plwin.listCtrl1.DeleteAllItems()
        self.currentsec = 0.0
    
        fl=event.GetFiles()
        for f in fl:
            r,e=os.path.splitext(f)
            if e.lower()=='.pls' or e.lower()=='.m3u':
                self.ParseM3U(f)
            else:
                self.plwin.appendItem(f ,'')

        self.fname='"'+self.plwin.listCtrl1.GetItem(0,1).GetText()+'"'
        self.fnamepos=0
        self.irany=1
        self.play_type=PLAY_LIST
        self.filename.SetLabel(self.fname[:44])
        self.timer1.Start(300)
        ##self.OnPlay(None)
        event.Skip()

    def msgh(self,msg):
        fname=self.unpackMsg(msg)
        self.infotext.AppendText('******Catch message:'+fname+'****\n')
        if fname== None:
            return
        self.fname=fname
        self.fnamepos=0
        self.irany=1
        self.play_type=PLAY_LIST
        self.plwin.listCtrl1.DeleteAllItems()

        r,e=os.path.splitext(self.fname)
        if e.lower()=='.pls"' or e.lower()=='.m3u"':
            self.ParseM3U(self.fname)
        else:
            self.plwin.appendItem(self.fname.strip('"'), self.subname.strip('"'))

        self.filename.SetLabel(self.fname[:44])
        self.timer1.Start(300)
        self.OnStop(None)
        self.OnPlay(None)


    def unpackMsg(self,msg):
        if msg[3] == 0:
            return ''
        sz = struct.calcsize('LLP')
        s = win32gui.PyMakeBuffer(sz,msg[3])
        dwData, cbData, lpData = struct.unpack('LLP', str(s))
        s = win32gui.PyMakeBuffer(cbData, lpData)
        return str(s)

    def OnEQ(self, event):
        try:
            if hasattr(self,"eqwin") == False:
                self.eqwin=EQFrame.create(self)
                x,y=self.GetPositionTuple()
                self.eqwin.SetPosition((x+387,y))
                self.eqwin.Show()
            else:
                self.eqwin.Hide()
                del(self.eqwin)
        except Exception, e:
            print e
            pass
        event.Skip()

    def OnWxMainMove(self, event):
        if hasattr(self,"eqwin"):
            x,y=self.GetPositionTuple()
            self.eqwin.SetPosition((x+387,y))
        if hasattr(self,"plwin"):
            x,y=self.GetPositionTuple()
            self.plwin.SetPosition((x+387,y))

        event.Skip()

    def OnTimeposLeftDown(self, event):
        self.remaing_time=not self.remaing_time
        event.Skip()

    def OnPlaylist(self, event):
        try:
            if self.plwin.IsShown() == False:
                x,y=self.GetPositionTuple()
                self.plwin.SetPosition((x+387,y))
                self.plwin.Show()
                self.play_type=PLAY_LIST
            else:
                self.plwin.Hide()
        except Exception, e:
            print e
            pass
        event.Skip()

    def ParseM3U(self, filename):
        filename=filename.strip('"\n\r\'')
        r,e=os.path.splitext(filename)
        dir=os.path.dirname(filename)
        if e.lower()=='.pls':
            ##parse pls file
            self.plwin.listCtrl1.DeleteAllItems()
            try:
                f=open(filename)
            except Exception, e:
                print e
                return
            lines=f.readlines(256)
            f.close()
            for line in lines:
                line=line.strip('"\n\r\'')
                self.plwin.append(line)

        elif e.lower()=='.m3u':
            ##parse m3u and extended m3u file
            self.plwin.listCtrl1.DeleteAllItems()
            try:
                f=open(filename)
            except Exception, e:
                print e
                return
            lines=f.readlines(256)
            f.close()
            for line in lines:
                line=line.strip('"\n\r\'')
                if line[0]=="#":
                    continue
                self.plwin.appendItem(dir+'\\'+line)
        else:
            pass

    def OnOpenURL(self, event):
        dlg = wx.TextEntryDialog(self, 'URL:', 'Open URL', '')
        dlg.CenterOnScreen()
        try:
            if dlg.ShowModal() == wx.ID_OK:
                answer = dlg.GetValue()
                self.timer1.Stop()
                self.fname='"'+answer+'"'
                self.subname=''
                self.filename.SetLabel(self.fname[:44])
                self.fnamepos=0
                self.irany=1
                self.plwin.listCtrl1.DeleteAllItems()
                self.play_type=PLAY_URL
                self.timer1.Start(300)
        finally:
            dlg.Destroy()
        event.Skip()
