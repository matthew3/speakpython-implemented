#Boa:Dialog:wxDialog1

##///////////////////////////////////////////////////////////////////////////
## Name:        FilesDialog.py
## Purpose:     File selection dialog of MPlayer Control
## Author:      Tamas Zelena
## Modified by:
## Created:     09/20/2004
## Copyright:   (c) Tamas Zelena
## Licence:     GNU-GPL
##///////////////////////////////////////////////////////////////////////////

import wx
import os

def create(parent):
    return wxDialog1(parent)

[wxID_WXDIALOG1, wxID_WXDIALOG1BUTTON1, wxID_WXDIALOG1BUTTON2, 
 wxID_WXDIALOG1COMBOBOX1, wxID_WXDIALOG1COMBOBOX2, 
 wxID_WXDIALOG1GENERICDIRCTRL1, wxID_WXDIALOG1GENERICDIRCTRL2, 
 wxID_WXDIALOG1LISTCTRL1, wxID_WXDIALOG1LISTCTRL2, wxID_WXDIALOG1NOTEBOOK1, 
 wxID_WXDIALOG1PANEL1, wxID_WXDIALOG1PANEL2, wxID_WXDIALOG1STATICLINE1, 
 wxID_WXDIALOG1STATICLINE2, wxID_WXDIALOG1STATICTEXT1, 
 wxID_WXDIALOG1STATICTEXT2, wxID_WXDIALOG1STATICTEXT3, 
 wxID_WXDIALOG1STATICTEXT4, wxID_WXDIALOG1SUBFILE_PATH, 
 wxID_WXDIALOG1VIDEOFILE_PATH, 
] = [wx.NewId() for _init_ctrls in range(20)]

class wxDialog1(wx.Dialog):
    def _init_coll_imageList1_Images(self, parent):
        # generated method, don't edit

        parent.Add(bitmap=wx.Bitmap('images/file.png', wx.BITMAP_TYPE_PNG),
              mask=wx.NullBitmap)
        parent.Add(bitmap=wx.Bitmap('images/videofile.png', wx.BITMAP_TYPE_PNG),
              mask=wx.NullBitmap)
        parent.Add(bitmap=wx.Bitmap('images/subtext.png', wx.BITMAP_TYPE_PNG),
              mask=wx.NullBitmap)
        parent.Add(bitmap=wx.Bitmap('images/audiofile.png', wx.BITMAP_TYPE_PNG),
              mask=wx.NullBitmap)

    def _init_coll_notebook1_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.panel1, select=True,
              text='Video/Audio File')
        parent.AddPage(imageId=-1, page=self.panel2, select=False,
              text='Subtitle File')

    def _init_utils(self):
        # generated method, don't edit
        self.imageList1 = wx.ImageList(height=16, width=16)
        self._init_coll_imageList1_Images(self.imageList1)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_WXDIALOG1, name='', parent=prnt,
              pos=wx.Point(341, 200), size=wx.Size(612, 478),
              style=wx.DEFAULT_DIALOG_STYLE, title='Open Media File')
        self._init_utils()
        self.SetClientSize(wx.Size(604, 451))

        self.notebook1 = wx.Notebook(id=wxID_WXDIALOG1NOTEBOOK1,
              name='notebook1', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(592, 376), style=0)
        self.notebook1.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.BOLD, False,
              'MS Shell Dlg'))

        self.panel1 = wx.Panel(id=wxID_WXDIALOG1PANEL1, name='panel1',
              parent=self.notebook1, pos=wx.Point(0, 0), size=wx.Size(584, 350),
              style=wx.TAB_TRAVERSAL)

        self.genericDirCtrl1 = wx.GenericDirCtrl(defaultFilter=0, dir='.',
              filter='*.*', id=wxID_WXDIALOG1GENERICDIRCTRL1,
              name='genericDirCtrl1', parent=self.panel1, pos=wx.Point(8, 8),
              size=wx.Size(216, 336),
              style=wx.DIRCTRL_DIR_ONLY | wx.SUNKEN_BORDER)

        self.button1 = wx.Button(id=wxID_WXDIALOG1BUTTON1, label='OK',
              name='button1', parent=self, pos=wx.Point(408, 408),
              size=wx.Size(75, 23), style=0)
        self.button1.Bind(wx.EVT_BUTTON, self.OnButton1Button,
              id=wxID_WXDIALOG1BUTTON1)

        self.listCtrl1 = wx.ListCtrl(id=wxID_WXDIALOG1LISTCTRL1,
              name='listCtrl1', parent=self.panel1, pos=wx.Point(232, 8),
              size=wx.Size(344, 224),
              style=wx.LC_LIST | wx.LC_SINGLE_SEL | wx.LC_ALIGN_TOP)
        self.listCtrl1.SetImageList(self.imageList1, wx.IMAGE_LIST_NORMAL)
        self.listCtrl1.SetImageList(self.imageList1, wx.IMAGE_LIST_SMALL)
        self.listCtrl1.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnListCtrl1ListItemActivated, id=wxID_WXDIALOG1LISTCTRL1)

        self.comboBox1 = wx.Choice(choices=['Video Files', 'Audio Files',
              'All Files', 'Media Files'], id=wxID_WXDIALOG1COMBOBOX1,
              name='comboBox1', parent=self.panel1, pos=wx.Point(424, 240),
              size=wx.Size(150, 21), style=0)
        self.comboBox1.SetStringSelection('Video Files')
        self.comboBox1.SetSelection(3)
        self.comboBox1.Bind(wx.EVT_CHOICE, self.OnComboBox1Choice,
              id=wxID_WXDIALOG1COMBOBOX1)

        self.staticText1 = wx.StaticText(id=wxID_WXDIALOG1STATICTEXT1,
              label='Filter:', name='staticText1', parent=self.panel1,
              pos=wx.Point(384, 244), size=wx.Size(33, 13), style=0)

        self.videofile_path = wx.TextCtrl(id=wxID_WXDIALOG1VIDEOFILE_PATH,
              name='videofile_path', parent=self.panel1, pos=wx.Point(240, 320),
              size=wx.Size(328, 21), style=wx.TE_READONLY, value='')

        self.staticLine1 = wx.StaticLine(id=wxID_WXDIALOG1STATICLINE1,
              name='staticLine1', parent=self.panel1, pos=wx.Point(232, 280),
              size=wx.Size(344, 2), style=0)

        self.staticText2 = wx.StaticText(id=wxID_WXDIALOG1STATICTEXT2,
              label='Selection:', name='staticText2', parent=self.panel1,
              pos=wx.Point(240, 296), size=wx.Size(64, 13), style=0)

        self.panel2 = wx.Panel(id=wxID_WXDIALOG1PANEL2, name='panel2',
              parent=self.notebook1, pos=wx.Point(0, 0), size=wx.Size(584, 350),
              style=wx.TAB_TRAVERSAL)

        self.genericDirCtrl2 = wx.GenericDirCtrl(defaultFilter=0, dir='.',
              filter='*.*', id=wxID_WXDIALOG1GENERICDIRCTRL2,
              name='genericDirCtrl2', parent=self.panel2, pos=wx.Point(8, 8),
              size=wx.Size(216, 336),
              style=wx.DIRCTRL_DIR_ONLY | wx.SUNKEN_BORDER)

        self.listCtrl2 = wx.ListCtrl(id=wxID_WXDIALOG1LISTCTRL2,
              name='listCtrl2', parent=self.panel2, pos=wx.Point(232, 8),
              size=wx.Size(344, 224),
              style=wx.LC_LIST | wx.LC_SINGLE_SEL | wx.LC_ALIGN_TOP)
        self.listCtrl2.SetImageList(self.imageList1, wx.IMAGE_LIST_SMALL)
        self.listCtrl2.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnListCtrl2ListItemActivated, id=wxID_WXDIALOG1LISTCTRL2)

        self.staticText4 = wx.StaticText(id=wxID_WXDIALOG1STATICTEXT4,
              label='Filter:', name='staticText4', parent=self.panel2,
              pos=wx.Point(384, 244), size=wx.Size(33, 13), style=0)

        self.comboBox2 = wx.Choice(choices=['Subtitle Files', 'All Files'],
              id=wxID_WXDIALOG1COMBOBOX2, name='comboBox2', parent=self.panel2,
              pos=wx.Point(424, 240), size=wx.Size(150, 21), style=0)
        self.comboBox2.SetStringSelection('Subtitle Files')
        self.comboBox2.Bind(wx.EVT_CHOICE, self.OnComboBox2Choice,
              id=wxID_WXDIALOG1COMBOBOX2)

        self.staticLine2 = wx.StaticLine(id=wxID_WXDIALOG1STATICLINE2,
              name='staticLine2', parent=self.panel2, pos=wx.Point(232, 280),
              size=wx.Size(344, 2), style=0)

        self.staticText3 = wx.StaticText(id=wxID_WXDIALOG1STATICTEXT3,
              label='Selection:', name='staticText3', parent=self.panel2,
              pos=wx.Point(240, 296), size=wx.Size(64, 13), style=0)

        self.subfile_path = wx.TextCtrl(id=wxID_WXDIALOG1SUBFILE_PATH,
              name='subfile_path', parent=self.panel2, pos=wx.Point(240, 320),
              size=wx.Size(328, 21), style=wx.TE_READONLY, value='')

        self.button2 = wx.Button(id=wxID_WXDIALOG1BUTTON2, label='Cancel',
              name='button2', parent=self, pos=wx.Point(512, 408),
              size=wx.Size(75, 23), style=0)
        self.button2.Bind(wx.EVT_BUTTON, self.OnButton2Button,
              id=wxID_WXDIALOG1BUTTON2)

        self._init_coll_notebook1_Pages(self.notebook1)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.Bind(wx.EVT_TREE_SEL_CHANGED, self.OnDirChanged, id=self.genericDirCtrl1.GetTreeCtrl().GetId())
        self.videofilter=['.avi','.mpg','.vob','.mpeg','.asf','.dat','.qt','.wmv','.mov','.rm','.rmvb']
        self.audiofilter=['.mp3','.ogg','.wma','.wav','.pls','.m3u']
        self.Bind(wx.EVT_TREE_SEL_CHANGED, self.OnDir2Changed, id=self.genericDirCtrl2.GetTreeCtrl().GetId())
        self.subfilter=['.srt','.sub','.txt']


    def OnDirChanged(self,event):
        self.DirChanged()
        event.Skip()

    def OnDir2Changed(self,event):
        self.Dir2Changed()
        event.Skip()

    def OnComboBox1Choice(self, event):
        self.DirChanged()
        event.Skip()

    def OnComboBox2Choice(self, event):
        self.Dir2Changed()
        event.Skip()

    def DirChanged(self):
        filtertype=self.comboBox1.GetSelection()
        self.listCtrl1.DeleteAllItems()
        dir=self.genericDirCtrl1.GetPath()
        diritem=os.listdir(dir)
        for item in diritem:
            if os.path.isfile(dir+'\\'+item):
                (name,ext)=os.path.splitext(item)
                if ext.lower() in self.videofilter:
                    if filtertype in [0,2,3]:
                        self.listCtrl1.InsertImageStringItem(self.listCtrl1.GetItemCount(),item,1)
                elif ext.lower() in self.audiofilter:
                    if filtertype in [1,2,3]:
                        self.listCtrl1.InsertImageStringItem(self.listCtrl1.GetItemCount(),item,3)
                elif filtertype==2:
                    self.listCtrl1.InsertImageStringItem(self.listCtrl1.GetItemCount(),item,0)

    def Dir2Changed(self):
        self.listCtrl2.DeleteAllItems()
        dir=self.genericDirCtrl2.GetPath()
        diritem=os.listdir(dir)
        for item in diritem:
            if os.path.isfile(dir+'\\'+item):
                (name,ext)=os.path.splitext(item)
                if ext.lower() in self.subfilter:
                    self.listCtrl2.InsertImageStringItem(self.listCtrl2.GetItemCount(),item,2)
                elif self.comboBox2.GetSelection()==1:
                    self.listCtrl2.InsertImageStringItem(self.listCtrl2.GetItemCount(),item,0)

    def OnListCtrl1ListItemActivated(self, event):
        self.currentItem = event.m_itemIndex
        itemtext=self.listCtrl1.GetItemText(self.currentItem)
        self.videofile_path.SetLabel(self.genericDirCtrl1.GetPath()+'\\'+itemtext)
        subtitlefile = os.path.splitext(self.genericDirCtrl1.GetPath()+'\\'+itemtext)[0]+'.srt'
        if os.path.isfile(subtitlefile):
            self.genericDirCtrl2.SetPath(subtitlefile)
            self.subfile_path.SetLabel(subtitlefile)

        event.Skip()

    def OnListCtrl2ListItemActivated(self, event):
        self.currentItem = event.m_itemIndex
        itemtext=self.listCtrl2.GetItemText(self.currentItem)
        self.subfile_path.SetLabel(self.genericDirCtrl2.GetPath()+'\\'+itemtext)
        event.Skip()

    def OnButton1Button(self, event):
        if not self.videofile_path.GetLabel():
            dlg = wx.MessageDialog(self, 'Please, first select a media file.',
              'Error', wx.OK | wx.ICON_ERROR)
            try:
                dlg.ShowModal()

            finally:
                dlg.Destroy()
        else:
            self.EndModal(wx.ID_OK)
        event.Skip()

    def OnButton2Button(self, event):
        self.EndModal(wx.ID_CANCEL)
        event.Skip()
