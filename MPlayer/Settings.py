#Boa:Dialog:settingsdlg

##///////////////////////////////////////////////////////////////////////////
## Name:        Settings.py
## Purpose:     Settings dialog of MPlayer Control
## Author:      Tamas Zelena
## Modified by:
## Created:     09/07/2004
## Copyright:   (c) Tamas Zelena
## Licence:     GNU-GPL
##///////////////////////////////////////////////////////////////////////////

import wx
import wx.html
import wx.grid
import winlirc

def create(parent):
    return settingsdlg(parent)

[wxID_SETTINGSDLG, wxID_SETTINGSDLGADV_CMD_LINE, wxID_SETTINGSDLGBUTTON1, 
 wxID_SETTINGSDLGCACHESIZE, wxID_SETTINGSDLGCANCELBUTTON, 
 wxID_SETTINGSDLGCHANNELS, wxID_SETTINGSDLGDEFVOLUME, wxID_SETTINGSDLGDELAY, 
 wxID_SETTINGSDLGDIRECTR, wxID_SETTINGSDLGDOUBLE, wxID_SETTINGSDLGDVD_A_LANG, 
 wxID_SETTINGSDLGDVD_S_LANG, wxID_SETTINGSDLGDV_VIEW, 
 wxID_SETTINGSDLGFONTPATH, wxID_SETTINGSDLGFRAMEDROP, 
 wxID_SETTINGSDLGHTMLWINDOW1, wxID_SETTINGSDLGIDX, wxID_SETTINGSDLGLIRCENABLE, 
 wxID_SETTINGSDLGLIRC_STATE, wxID_SETTINGSDLGLISTCTRL1, 
 wxID_SETTINGSDLGNORMLV, wxID_SETTINGSDLGNOTEBOOK1, wxID_SETTINGSDLGOKBUTTON, 
 wxID_SETTINGSDLGOSDBGALPHA, wxID_SETTINGSDLGOSDBGGRAY, 
 wxID_SETTINGSDLGOSDBLACKBAND, wxID_SETTINGSDLGOSDPOS, 
 wxID_SETTINGSDLGOVERLAPPING, wxID_SETTINGSDLGPANEL1, wxID_SETTINGSDLGPANEL2, 
 wxID_SETTINGSDLGPANEL3, wxID_SETTINGSDLGPANEL4, wxID_SETTINGSDLGPANEL5, 
 wxID_SETTINGSDLGPANEL6, wxID_SETTINGSDLGPANSCAN, 
 wxID_SETTINGSDLGPANSCANVALUE, wxID_SETTINGSDLGPOSTP, 
 wxID_SETTINGSDLGSTATICLINE1, wxID_SETTINGSDLGSTATICLINE2, 
 wxID_SETTINGSDLGSTATICLINE3, wxID_SETTINGSDLGSTATICLINE4, 
 wxID_SETTINGSDLGSTATICLINE5, wxID_SETTINGSDLGSTATICLINE6, 
 wxID_SETTINGSDLGSTATICLINE7, wxID_SETTINGSDLGSTATICLINE8, 
 wxID_SETTINGSDLGSTATICTEXT1, wxID_SETTINGSDLGSTATICTEXT10, 
 wxID_SETTINGSDLGSTATICTEXT11, wxID_SETTINGSDLGSTATICTEXT12, 
 wxID_SETTINGSDLGSTATICTEXT13, wxID_SETTINGSDLGSTATICTEXT14, 
 wxID_SETTINGSDLGSTATICTEXT15, wxID_SETTINGSDLGSTATICTEXT16, 
 wxID_SETTINGSDLGSTATICTEXT17, wxID_SETTINGSDLGSTATICTEXT18, 
 wxID_SETTINGSDLGSTATICTEXT2, wxID_SETTINGSDLGSTATICTEXT3, 
 wxID_SETTINGSDLGSTATICTEXT4, wxID_SETTINGSDLGSTATICTEXT6, 
 wxID_SETTINGSDLGSTATICTEXT7, wxID_SETTINGSDLGSTATICTEXT8, 
 wxID_SETTINGSDLGSTATICTEXT9, wxID_SETTINGSDLGUSE_CACHE, 
 wxID_SETTINGSDLGVOCHOICE, 
] = [wx.NewId() for _init_ctrls in range(64)]

def Boolean(text):
    if text=='True': return True
    if text=='False': return False
    raise Exception, 'Boolean error'

[wxID_SETTINGSDLGTIMER1] = [wx.NewId() for _init_utils in range(1)]

class settingsdlg(wx.Dialog):
    def _init_coll_listCtrl1_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_CENTER,
              heading='Function', width=162)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_CENTER,
              heading='IR Button', width=162)

    def _init_coll_notebook1_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.panel2, select=True, text='Video')
        parent.AddPage(imageId=-1, page=self.panel3, select=False, text='Audio')
        parent.AddPage(imageId=-1, page=self.panel1, select=False, text='OSD')
        parent.AddPage(imageId=-1, page=self.panel5, select=False,
              text='WinLirc')
        parent.AddPage(imageId=-1, page=self.panel4, select=False, text='DVD')
        parent.AddPage(imageId=-1, page=self.panel6, select=False,
              text='Advanced')

    def _init_utils(self):
        # generated method, don't edit
        self.timer1 = wx.Timer(id=wxID_SETTINGSDLGTIMER1, owner=self)
        self.Bind(wx.EVT_TIMER, self.OnTimer1Timer, id=wxID_SETTINGSDLGTIMER1)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_SETTINGSDLG, name='settingsdlg',
              parent=prnt, pos=wx.Point(434, 384), size=wx.Size(392, 402),
              style=wx.DEFAULT_DIALOG_STYLE, title='Settings')
        self._init_utils()
        self.SetClientSize(wx.Size(384, 375))
        self.Bind(wx.EVT_CLOSE, self.OnSettingsdlgClose)

        self.notebook1 = wx.Notebook(id=wxID_SETTINGSDLGNOTEBOOK1,
              name='notebook1', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(368, 312), style=0)

        self.okbutton = wx.Button(id=wxID_SETTINGSDLGOKBUTTON, label='OK',
              name='okbutton', parent=self, pos=wx.Point(64, 336),
              size=wx.Size(75, 23), style=0)
        self.okbutton.Bind(wx.EVT_BUTTON, self.OnOKButton,
              id=wxID_SETTINGSDLGOKBUTTON)

        self.cancelbutton = wx.Button(id=wxID_SETTINGSDLGCANCELBUTTON,
              label='Cancel', name='cancelbutton', parent=self,
              pos=wx.Point(272, 336), size=wx.Size(75, 23), style=0)
        self.cancelbutton.Bind(wx.EVT_BUTTON, self.OnCancelButton,
              id=wxID_SETTINGSDLGCANCELBUTTON)

        self.panel1 = wx.Panel(id=wxID_SETTINGSDLGPANEL1, name='panel1',
              parent=self.notebook1, pos=wx.Point(0, 0), size=wx.Size(360, 286),
              style=wx.TAB_TRAVERSAL)

        self.panel2 = wx.Panel(id=wxID_SETTINGSDLGPANEL2, name='panel2',
              parent=self.notebook1, pos=wx.Point(0, 0), size=wx.Size(360, 286),
              style=wx.TAB_TRAVERSAL)

        self.panel3 = wx.Panel(id=wxID_SETTINGSDLGPANEL3, name='panel3',
              parent=self.notebook1, pos=wx.Point(0, 0), size=wx.Size(360, 286),
              style=wx.TAB_TRAVERSAL)

        self.double = wx.CheckBox(id=wxID_SETTINGSDLGDOUBLE,
              label='Double Buffering', name='double', parent=self.panel2,
              pos=wx.Point(112, 80), size=wx.Size(104, 13), style=0)
        self.double.SetValue(True)

        self.directr = wx.CheckBox(id=wxID_SETTINGSDLGDIRECTR,
              label='Direct Rendering', name='directr', parent=self.panel2,
              pos=wx.Point(112, 104), size=wx.Size(104, 13), style=0)
        self.directr.SetValue(False)

        self.staticText1 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT1,
              label='KByte', name='staticText1', parent=self.panel2,
              pos=wx.Point(200, 260), size=wx.Size(28, 13), style=0)

        self.vochoice = wx.ComboBox(choices=['directx', 'gl2', 'sdl'],
              id=wxID_SETTINGSDLGVOCHOICE, name='vochoice', parent=self.panel2,
              pos=wx.Point(152, 24), size=wx.Size(96, 21), style=0,
              value='directx')
        self.vochoice.SetStringSelection('directx')

        self.framedrop = wx.CheckBox(id=wxID_SETTINGSDLGFRAMEDROP,
              label='Framedrop', name='framedrop', parent=self.panel2,
              pos=wx.Point(112, 128), size=wx.Size(73, 13), style=0)
        self.framedrop.SetValue(False)

        self.idx = wx.CheckBox(id=wxID_SETTINGSDLGIDX,
              label='Rebuild AVI index', name='idx', parent=self.panel2,
              pos=wx.Point(112, 152), size=wx.Size(112, 13), style=0)
        self.idx.SetValue(False)

        self.postp = wx.CheckBox(id=wxID_SETTINGSDLGPOSTP,
              label='Post Processing', name='postp', parent=self.panel2,
              pos=wx.Point(112, 176), size=wx.Size(112, 13), style=0)
        self.postp.SetValue(False)

        self.normlv = wx.CheckBox(id=wxID_SETTINGSDLGNORMLV,
              label='Normalize volume', name='normlv', parent=self.panel3,
              pos=wx.Point(48, 32), size=wx.Size(112, 13), style=0)
        self.normlv.SetValue(False)

        self.defvolume = wx.Slider(id=wxID_SETTINGSDLGDEFVOLUME, maxValue=20,
              minValue=-20, name='defvolume', parent=self.panel3,
              point=wx.Point(119, 80), size=wx.Size(203, 27),
              style=wx.SL_HORIZONTAL, value=0)
        self.defvolume.SetLabel('')
        self.defvolume.Bind(wx.EVT_COMMAND_SCROLL, self.OnDefvolumeScroll,
              id=wxID_SETTINGSDLGDEFVOLUME)

        self.staticText2 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT2,
              label='Default volume gain:', name='staticText2',
              parent=self.panel3, pos=wx.Point(48, 64), size=wx.Size(104, 13),
              style=0)

        self.channels = wx.ComboBox(choices=['Stereo', '4-speakers',
              '5.1-speakers'], id=wxID_SETTINGSDLGCHANNELS, name='channels',
              parent=self.panel3, pos=wx.Point(104, 140), size=wx.Size(90, 21),
              style=wx.CB_READONLY, value='Stereo')
        self.channels.SetLabel('Stereo')

        self.staticText3 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT3,
              label='Delay:', name='staticText3', parent=self.panel3,
              pos=wx.Point(48, 200), size=wx.Size(30, 13), style=0)

        self.staticLine1 = wx.StaticLine(id=wxID_SETTINGSDLGSTATICLINE1,
              name='staticLine1', parent=self.panel3, pos=wx.Point(8, 176),
              size=wx.Size(336, 2), style=0)

        self.staticLine3 = wx.StaticLine(id=wxID_SETTINGSDLGSTATICLINE3,
              name='staticLine3', parent=self.panel3, pos=wx.Point(8, 56),
              size=wx.Size(336, 2), style=0)

        self.staticLine4 = wx.StaticLine(id=wxID_SETTINGSDLGSTATICLINE4,
              name='staticLine4', parent=self.panel2, pos=wx.Point(16, 64),
              size=wx.Size(328, 2), style=0)

        self.staticText6 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT6,
              label='Font path:', name='staticText6', parent=self.panel1,
              pos=wx.Point(24, 32), size=wx.Size(48, 13), style=0)

        self.fontpath = wx.TextCtrl(id=wxID_SETTINGSDLGFONTPATH,
              name='fontpath', parent=self.panel1, pos=wx.Point(80, 24),
              size=wx.Size(208, 21), style=0,
              value='mplayer\\font\\arial-18\\font.desc')

        self.staticLine5 = wx.StaticLine(id=wxID_SETTINGSDLGSTATICLINE5,
              name='staticLine5', parent=self.panel1, pos=wx.Point(16, 176),
              size=wx.Size(328, 2), style=0)

        self.overlapping = wx.CheckBox(id=wxID_SETTINGSDLGOVERLAPPING,
              label='Overlapping', name='overlapping', parent=self.panel1,
              pos=wx.Point(224, 88), size=wx.Size(80, 13), style=0)
        self.overlapping.SetValue(False)

        self.osdpos = wx.ComboBox(choices=['Top', 'Center', 'Bottom' ],
              id=wxID_SETTINGSDLGOSDPOS, name='osdpos', parent=self.panel1,
              pos=wx.Point(88, 136), size=wx.Size(80, 21), style=wx.CB_READONLY,
              value='Bottom')
        self.osdpos.SetLabel('Bottom')

        self.staticText7 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT7,
              label='Position:', name='staticText7', parent=self.panel1,
              pos=wx.Point(32, 144), size=wx.Size(40, 13), style=0)

        self.staticLine6 = wx.StaticLine(id=wxID_SETTINGSDLGSTATICLINE6,
              name='staticLine6', parent=self.panel1, pos=wx.Point(16, 64),
              size=wx.Size(328, 2), style=0)

        self.staticLine2 = wx.StaticLine(id=wxID_SETTINGSDLGSTATICLINE2,
              name='staticLine2', parent=self.panel3, pos=wx.Point(8, 112),
              size=wx.Size(336, 2), style=0)

        self.staticText8 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT8,
              label='Channels:', name='staticText8', parent=self.panel3,
              pos=wx.Point(48, 144), size=wx.Size(47, 13), style=0)

        self.delay = wx.SpinCtrl(id=wxID_SETTINGSDLGDELAY, initial=0, max=200,
              min=-200, name='delay', parent=self.panel3, pos=wx.Point(88, 195),
              size=wx.Size(50, 21), style=wx.SP_ARROW_KEYS)
        self.delay.SetValue(0)
        self.delay.SetRange(-200, 200)

        self.staticText9 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT9,
              label='x 100 msec', name='staticText9', parent=self.panel3,
              pos=wx.Point(144, 200), size=wx.Size(54, 13), style=0)

        self.panscan = wx.CheckBox(id=wxID_SETTINGSDLGPANSCAN,
              label='Pan&&Scan emu.', name='panscan', parent=self.panel2,
              pos=wx.Point(112, 201), size=wx.Size(96, 13), style=0)
        self.panscan.SetValue(False)
        self.panscan.Bind(wx.EVT_CHECKBOX, self.OnPanscanCheckbox,
              id=wxID_SETTINGSDLGPANSCAN)

        self.panscanvalue = wx.SpinCtrl(id=wxID_SETTINGSDLGPANSCANVALUE,
              initial=0, max=2048, min=0, name='panscanvalue',
              parent=self.panel2, pos=wx.Point(136, 217), size=wx.Size(48, 21),
              style=wx.SP_ARROW_KEYS)
        self.panscanvalue.Enable(False)
        self.panscanvalue.SetValue(0)

        self.staticText15 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT15,
              label='Video Output:', name='staticText15', parent=self.panel2,
              pos=wx.Point(80, 27), size=wx.Size(72, 13), style=0)

        self.panel5 = wx.Panel(id=wxID_SETTINGSDLGPANEL5, name='panel5',
              parent=self.notebook1, pos=wx.Point(0, 0), size=wx.Size(360, 286),
              style=wx.TAB_TRAVERSAL)

        self.listCtrl1 = wx.ListCtrl(id=wxID_SETTINGSDLGLISTCTRL1,
              name='listCtrl1', parent=self.panel5, pos=wx.Point(16, 80),
              size=wx.Size(328, 168), style=wx.LC_SINGLE_SEL | wx.LC_REPORT)
        self._init_coll_listCtrl1_Columns(self.listCtrl1)
        self.listCtrl1.Bind(wx.EVT_LIST_ITEM_ACTIVATED,
              self.OnListCtrl1ListItemActivated, id=wxID_SETTINGSDLGLISTCTRL1)
        self.listCtrl1.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnListCtrl1ListItemDeselected, id=wxID_SETTINGSDLGLISTCTRL1)

        self.lircenable = wx.CheckBox(id=wxID_SETTINGSDLGLIRCENABLE,
              label='Enable WinLirc Control', name='lircenable',
              parent=self.panel5, pos=wx.Point(128, 32), size=wx.Size(136, 13),
              style=0)
        self.lircenable.SetValue(False)
        self.lircenable.Bind(wx.EVT_CHECKBOX, self.OnLircenableCheckbox,
              id=wxID_SETTINGSDLGLIRCENABLE)

        self.staticText16 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT16,
              label='WinLirc client :', name='staticText16', parent=self.panel5,
              pos=wx.Point(24, 264), size=wx.Size(70, 13), style=0)

        self.lirc_state = wx.StaticText(id=wxID_SETTINGSDLGLIRC_STATE,
              label='disconnected', name='lirc_state', parent=self.panel5,
              pos=wx.Point(104, 264), size=wx.Size(81, 16), style=0)
        self.lirc_state.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.NORMAL,
              True, 'Microsoft Sans Serif'))

        self.panel4 = wx.Panel(id=wxID_SETTINGSDLGPANEL4, name='panel4',
              parent=self.notebook1, pos=wx.Point(0, 0), size=wx.Size(360, 286),
              style=wx.TAB_TRAVERSAL)

        self.staticText11 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT11,
              label='Prefered DVD Audio Language:', name='staticText11',
              parent=self.panel4, pos=wx.Point(32, 48), size=wx.Size(150, 13),
              style=0)

        self.staticText12 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT12,
              label='Prefered DVD Subtitle Language:', name='staticText12',
              parent=self.panel4, pos=wx.Point(24, 96), size=wx.Size(158, 13),
              style=0)

        self.dvd_a_lang = wx.ComboBox(choices=['en', 'hu', 'de', 'fr', 'es',
              'jp', 'sk', 'cz', 'no'], id=wxID_SETTINGSDLGDVD_A_LANG,
              name='dvd_a_lang', parent=self.panel4, pos=wx.Point(192, 40),
              size=wx.Size(56, 21), style=0, value='en')
        self.dvd_a_lang.SetLabel('en')
        self.dvd_a_lang.SetStringSelection('en')

        self.dvd_s_lang = wx.ComboBox(choices=['none', 'en', 'hu', 'de', 'fr',
              'es', 'jp', 'sk', 'cz', 'no'], id=wxID_SETTINGSDLGDVD_S_LANG,
              name='dvd_s_lang', parent=self.panel4, pos=wx.Point(192, 88),
              size=wx.Size(56, 21), style=0, value='none')
        self.dvd_s_lang.SetLabel('none')
        self.dvd_s_lang.SetStringSelection('none')

        self.staticText10 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT10,
              label='(to work need restart)', name='staticText10',
              parent=self.panel5, pos=wx.Point(144, 48), size=wx.Size(120, 13),
              style=0)

        self.use_cache = wx.CheckBox(id=wxID_SETTINGSDLGUSE_CACHE,
              label='Use Cache', name='use_cache', parent=self.panel2,
              pos=wx.Point(112, 240), size=wx.Size(73, 13), style=0)
        self.use_cache.SetValue(False)
        self.use_cache.Bind(wx.EVT_CHECKBOX, self.OnUse_cacheCheckbox,
              id=wxID_SETTINGSDLGUSE_CACHE)

        self.cachesize = wx.ComboBox(choices=['2048', '4096', '8192', '16384'],
              id=wxID_SETTINGSDLGCACHESIZE, name='cachesize',
              parent=self.panel2, pos=wx.Point(136, 256), size=wx.Size(56, 21),
              style=0, value='4096')
        self.cachesize.SetLabel('4096')
        self.cachesize.Enable(False)

        self.staticText13 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT13,
              label='pixel crop', name='staticText13', parent=self.panel2,
              pos=wx.Point(192, 222), size=wx.Size(45, 13), style=0)

        self.panel6 = wx.Panel(id=wxID_SETTINGSDLGPANEL6, name='panel6',
              parent=self.notebook1, pos=wx.Point(0, 0), size=wx.Size(360, 286),
              style=wx.TAB_TRAVERSAL)

        self.adv_cmd_line = wx.TextCtrl(id=wxID_SETTINGSDLGADV_CMD_LINE,
              name='adv_cmd_line', parent=self.panel6, pos=wx.Point(48, 40),
              size=wx.Size(264, 88), style=wx.TE_MULTILINE | wx.TE_WORDWRAP,
              value='')

        self.staticText14 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT14,
              label='Advanced command line option:', name='staticText14',
              parent=self.panel6, pos=wx.Point(8, 16), size=wx.Size(152, 13),
              style=0)

        self.htmlWindow1 = wx.html.HtmlWindow(id=wxID_SETTINGSDLGHTMLWINDOW1,
              name='htmlWindow1', parent=self.panel6, pos=wx.Point(32, 136),
              size=wx.Size(296, 144), style=0)
        self.htmlWindow1.SetBackgroundColour(wx.Colour(212, 208, 200))

        self.staticLine7 = wx.StaticLine(id=wxID_SETTINGSDLGSTATICLINE7,
              name='staticLine7', parent=self.panel1, pos=wx.Point(16, 120),
              size=wx.Size(328, 2), style=0)

        self.staticText17 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT17,
              label='Background transparency:', name='staticText17',
              parent=self.panel1, pos=wx.Point(32, 192), size=wx.Size(136, 13),
              style=0)

        self.staticText18 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT18,
              label='Background intensity:', name='staticText18',
              parent=self.panel1, pos=wx.Point(32, 240), size=wx.Size(120, 13),
              style=0)

        self.osdbggray = wx.Slider(id=wxID_SETTINGSDLGOSDBGGRAY, maxValue=255,
              minValue=0, name='osdbggray', parent=self.panel1,
              point=wx.Point(67, 256), size=wx.Size(230, 20),
              style=wx.SL_HORIZONTAL, value=0)

        self.osdbgalpha = wx.Slider(id=wxID_SETTINGSDLGOSDBGALPHA, maxValue=0,
              minValue=-254, name='osdbgalpha', parent=self.panel1,
              point=wx.Point(67, 208), size=wx.Size(230, 20),
              style=wx.SL_HORIZONTAL, value=0)
        self.osdbgalpha.SetLabel('')

        self.staticLine8 = wx.StaticLine(id=wxID_SETTINGSDLGSTATICLINE8,
              name='staticLine8', parent=self.panel1, pos=wx.Point(184, 72),
              size=wx.Size(2, 40), style=wx.LI_HORIZONTAL)

        self.osdblackband = wx.CheckBox(id=wxID_SETTINGSDLGOSDBLACKBAND,
              label='Use black band', name='osdblackband', parent=self.panel1,
              pos=wx.Point(48, 88), size=wx.Size(96, 13), style=0)
        self.osdblackband.SetValue(False)

        self.button1 = wx.Button(id=wxID_SETTINGSDLGBUTTON1, label='...',
              name='button1', parent=self.panel1, pos=wx.Point(296, 24),
              size=wx.Size(30, 23), style=0)
        self.button1.Bind(wx.EVT_BUTTON, self.OnFontBrowse,
              id=wxID_SETTINGSDLGBUTTON1)

        self.staticText4 = wx.StaticText(id=wxID_SETTINGSDLGSTATICTEXT4,
              label='dB', name='staticText4', parent=self.panel3,
              pos=wx.Point(96, 88), size=wx.Size(13, 13), style=0)

        self.dv_view = wx.TextCtrl(id=wxID_SETTINGSDLGDV_VIEW, name='dv_view',
              parent=self.panel3, pos=wx.Point(56, 80), size=wx.Size(32, 21),
              style=wx.TE_CENTER | wx.TE_READONLY, value='0')

        self._init_coll_notebook1_Pages(self.notebook1)

    def __init__(self, parent):
        self._init_ctrls(parent)
        x,y=self.GetSizeTuple()
        self.SetPosition((wx.SystemSettings_GetMetric(wx.SYS_SCREEN_X)/2-x/2,wx.SystemSettings_GetMetric(wx.SYS_SCREEN_Y)/2-y/2))
        self.listCtrl1.InsertStringItem(0,'osd')
        self.listCtrl1.SetStringItem(0,1,'<db click & press IR>')
        self.listCtrl1.InsertStringItem(0,'exit')
        self.listCtrl1.SetStringItem(0,1,'<db click & press IR>')
        self.listCtrl1.InsertStringItem(0,'fullscreen')
        self.listCtrl1.SetStringItem(0,1,'<db click & press IR>')
        self.listCtrl1.InsertStringItem(0,'seek-')
        self.listCtrl1.SetStringItem(0,1,'<db click & press IR>')
        self.listCtrl1.InsertStringItem(0,'seek+')
        self.listCtrl1.SetStringItem(0,1,'<db click & press IR>')
        self.listCtrl1.InsertStringItem(0,'vol. down')
        self.listCtrl1.SetStringItem(0,1,'<db click & press IR>')
        self.listCtrl1.InsertStringItem(0,'vol. up')
        self.listCtrl1.SetStringItem(0,1,'<db click & press IR>')
        self.listCtrl1.InsertStringItem(0,'mute')
        self.listCtrl1.SetStringItem(0,1,'<db click & press IR>')
        self.listCtrl1.InsertStringItem(0,'pause')
        self.listCtrl1.SetStringItem(0,1,'<db click & press IR>')
        self.listCtrl1.InsertStringItem(0,'stop')
        self.listCtrl1.SetStringItem(0,1,'<db click & press IR>')
        self.listCtrl1.InsertStringItem(0,'play')
        self.listCtrl1.SetStringItem(0,1,'<db click & press IR>')
        self.winlirc=None
        self.loadcfg()
        self.panscanvalue.Enable(self.panscan.IsChecked())
        self.listCtrl1.Enable(self.lircenable.IsChecked())
        self.cachesize.Enable(self.use_cache.IsChecked())
        self.OnLircenableCheckbox(None)
        self.htmlWindow1.SetPage(
        '<html>\
        <body BGCOLOR="#D4D0C8">\
        <b>Warning</b><br>\
        Please, don\'t use the following<br>\
        command line arguments:<br>\
        <ul><li>quiet or really-quiet</li><li>v or verbose</li><li>slave</li></ul>\
        </body>\
        </html>')

    def OnOKButton(self, event):
        event.Skip()
        self.savecfg()
        self.Close()

    def OnCancelButton(self, event):
        event.Skip()
        self.Close()

    def loadcfg(self):
        opt=[]
        try:
            cfgfile=open('mplayerc.cfg','r')

            line=cfgfile.readline()
            while(len(line) > 0) :
                line=line.rstrip('\n')
                line=line.rstrip('\r')
                if len(line) > 0 and line.find('=') > -1:
                    ##opt.append(tuple(line.split('=')))
                    opt.append((line[:line.find('=')],line[line.find('=')+1:]))
                line=cfgfile.readline()
            cfgfile.close()
            options=dict(opt)
            ##Video options
            if options.has_key('vo'):
                ##self.vochoice.SetStringSelection(options['vo'])
                self.vochoice.SetValue(options['vo'])
            if options.has_key('double'):
                self.double.SetValue(Boolean(options['double']))
            if options.has_key('dr'):
                self.directr.SetValue(Boolean(options['dr']))
            if options.has_key('framedrop'):
                self.framedrop.SetValue(Boolean(options['framedrop']))
            if options.has_key('idx'):
                self.idx.SetValue(Boolean(options['idx']))
            if options.has_key('postp'):
                self.postp.SetValue(Boolean(options['postp']))
            if options.has_key('panscan'):
                self.panscan.SetValue(Boolean(options['panscan']))
                if options.has_key('panscanvalue') and self.panscan.IsChecked():
                    self.panscanvalue.SetValue(int(options['panscanvalue']))
            if options.has_key('usecache'):
                self.use_cache.SetValue(Boolean(options['usecache']))
                if options.has_key('cachesize') and self.use_cache.IsChecked():
                    self.cachesize.SetValue(options['cachesize'])
            ##Audio options
            if options.has_key('normvol'):
                self.normlv.SetValue(Boolean(options['normvol']))
            if options.has_key('defvol'):
                self.defvolume.SetValue(int(options['defvol']))
                self.dv_view.SetValue(str(options['defvol']))
            if options.has_key('channels'):
                self.channels.SetStringSelection(options['channels'])
            if options.has_key('audiodelay'):
                self.delay.SetValue(int(options['audiodelay']))
            ##OSD options
            if options.has_key('fontpath'):
                self.fontpath.SetValue(options['fontpath'])
            if options.has_key('overlapping'):
                self.overlapping.SetValue(Boolean(options['overlapping']))
            if options.has_key('osdblackband'):
                self.osdblackband.SetValue(Boolean(options['osdblackband']))
            if options.has_key('osdpos'):
                self.osdpos.SetStringSelection(options['osdpos'])
            if options.has_key('osdbgalpha'):
                self.osdbgalpha.SetValue(int(options['osdbgalpha'])-255)
            if options.has_key('osdbggray'):
                self.osdbggray.SetValue(int(options['osdbggray']))

            ##WinLirc
            if options.has_key('winlirc'):
                self.lircenable.SetValue(Boolean(options['winlirc']))
            if options.has_key('winlirc_play'):
                item=self.listCtrl1.FindItem(-1,'play')
                self.listCtrl1.SetStringItem(item,1,options['winlirc_play'])
            if options.has_key('winlirc_stop'):
                item=self.listCtrl1.FindItem(-1,'stop')
                self.listCtrl1.SetStringItem(item,1,options['winlirc_stop'])
            if options.has_key('winlirc_pause'):
                item=self.listCtrl1.FindItem(-1,'pause')
                self.listCtrl1.SetStringItem(item,1,options['winlirc_pause'])
            if options.has_key('winlirc_mute'):
                item=self.listCtrl1.FindItem(-1,'mute')
                self.listCtrl1.SetStringItem(item,1,options['winlirc_mute'])
            if options.has_key('winlirc_vol_up'):
                item=self.listCtrl1.FindItem(-1,'vol. up')
                self.listCtrl1.SetStringItem(item,1,options['winlirc_vol_up'])
            if options.has_key('winlirc_vol_down'):
                item=self.listCtrl1.FindItem(-1,'vol. down')
                self.listCtrl1.SetStringItem(item,1,options['winlirc_vol_down'])
            if options.has_key('winlirc_seek_+'):
                item=self.listCtrl1.FindItem(-1,'seek+')
                self.listCtrl1.SetStringItem(item,1,options['winlirc_seek_+'])
            if options.has_key('winlirc_seek_-'):
                item=self.listCtrl1.FindItem(-1,'seek-')
                self.listCtrl1.SetStringItem(item,1,options['winlirc_seek_-'])
            if options.has_key('winlirc_fs'):
                item=self.listCtrl1.FindItem(-1,'fullscreen')
                self.listCtrl1.SetStringItem(item,1,options['winlirc_fs'])
            if options.has_key('winlirc_exit'):
                item=self.listCtrl1.FindItem(-1,'exit')
                self.listCtrl1.SetStringItem(item,1,options['winlirc_exit'])
            if options.has_key('winlirc_osd'):
                item=self.listCtrl1.FindItem(-1,'osd')
                self.listCtrl1.SetStringItem(item,1,options['winlirc_osd'])
            ##DVD
            if options.has_key('dvd_alang'):
                self.dvd_a_lang.SetValue(options['dvd_alang'])
            if options.has_key('dvd_slang'):
                self.dvd_s_lang.SetValue(options['dvd_slang'])
            ##Advanced cmd.line
            if options.has_key('adv_cmd'):
                self.adv_cmd_line.SetValue(options['adv_cmd'])
                self.adv_cmd_line.SetInsertionPointEnd()

        except Exception, e:
            print e
            cfgfile.close()

    def savecfg(self):
        cfgfile=open('mplayerc.cfg','w')
        ##Video options
        cfgfile.write('vo='+self.vochoice.GetValue()+'\n')
        cfgfile.write('double='+str(self.double.IsChecked())+'\n')
        cfgfile.write('dr='+str(self.directr.IsChecked())+'\n')
        cfgfile.write('framedrop='+str(self.framedrop.IsChecked())+'\n')
        cfgfile.write('idx='+str(self.idx.IsChecked())+'\n')
        cfgfile.write('postp='+str(self.postp.IsChecked())+'\n')
        cfgfile.write('panscan='+str(self.panscan.IsChecked())+'\n')
        cfgfile.write('panscanvalue='+str(self.panscanvalue.GetValue())+'\n')
        cfgfile.write('usecache='+str(self.use_cache.IsChecked())+'\n')
        cfgfile.write('cachesize='+self.cachesize.GetValue()+'\n')
        ##Audio options
        cfgfile.write('normvol='+str(self.normlv.IsChecked())+'\n')
        cfgfile.write('defvol='+str(self.defvolume.GetValue())+'\n')
        cfgfile.write('channels='+str(self.channels.GetValue())+'\n')
        cfgfile.write('audiodelay='+str(self.delay.GetValue())+'\n')
        ##OSD/SUB options
        cfgfile.write('fontpath='+self.fontpath.GetValue()+'\n')
        cfgfile.write('overlapping='+str(self.overlapping.IsChecked())+'\n')
        cfgfile.write('osdpos='+str(self.osdpos.GetValue())+'\n')
        cfgfile.write('osdbgalpha='+str(self.osdbgalpha.GetValue()+255)+'\n')
        cfgfile.write('osdbggray='+str(self.osdbggray.GetValue())+'\n')
        cfgfile.write('osdblackband='+str(self.osdblackband.IsChecked())+'\n')
        ##WinLirc
        cfgfile.write('winlirc='+str(self.lircenable.IsChecked())+'\n')
        item=self.listCtrl1.FindItem(-1,'play')
        cfgfile.write('winlirc_play='+self.listCtrl1.GetItem(item,1).GetText()+'\n')
        item=self.listCtrl1.FindItem(-1,'stop')
        cfgfile.write('winlirc_stop='+self.listCtrl1.GetItem(item,1).GetText()+'\n')
        item=self.listCtrl1.FindItem(-1,'pause')
        cfgfile.write('winlirc_pause='+self.listCtrl1.GetItem(item,1).GetText()+'\n')
        item=self.listCtrl1.FindItem(-1,'mute')
        cfgfile.write('winlirc_mute='+self.listCtrl1.GetItem(item,1).GetText()+'\n')
        item=self.listCtrl1.FindItem(-1,'vol. up')
        cfgfile.write('winlirc_vol_up='+self.listCtrl1.GetItem(item,1).GetText()+'\n')
        item=self.listCtrl1.FindItem(-1,'vol. down')
        cfgfile.write('winlirc_vol_down='+self.listCtrl1.GetItem(item,1).GetText()+'\n')
        item=self.listCtrl1.FindItem(-1,'seek+')
        cfgfile.write('winlirc_seek_+='+self.listCtrl1.GetItem(item,1).GetText()+'\n')
        item=self.listCtrl1.FindItem(-1,'seek-')
        cfgfile.write('winlirc_seek_-='+self.listCtrl1.GetItem(item,1).GetText()+'\n')
        item=self.listCtrl1.FindItem(-1,'fullscreen')
        cfgfile.write('winlirc_fs='+self.listCtrl1.GetItem(item,1).GetText()+'\n')
        item=self.listCtrl1.FindItem(-1,'exit')
        cfgfile.write('winlirc_exit='+self.listCtrl1.GetItem(item,1).GetText()+'\n')
        item=self.listCtrl1.FindItem(-1,'osd')
        cfgfile.write('winlirc_osd='+self.listCtrl1.GetItem(item,1).GetText()+'\n')
        ##DVD
        cfgfile.write('dvd_alang='+str(self.dvd_a_lang.GetValue())+'\n')
        cfgfile.write('dvd_slang='+str(self.dvd_s_lang.GetValue())+'\n')
        ##Advanced cmd.line
        cfgfile.write('adv_cmd='+str(self.adv_cmd_line.GetValue()+'\n'))


        cfgfile.close()

    def OnPanscanCheckbox(self, event):
        self.panscanvalue.Enable(self.panscan.IsChecked())
        event.Skip()

    def OnListCtrl1ListItemActivated(self, event):
        self.currentItem = event.m_itemIndex
        itemtext=self.listCtrl1.GetItemText(self.currentItem)
        self.listCtrl1.SetStringItem(self.currentItem,1,'.......')
        event.Skip()

    def OnLircenableCheckbox(self, event):
        self.listCtrl1.Enable(self.lircenable.IsChecked())
        if self.lircenable.IsChecked() == True:
            self.winlirc=winlirc.winlirc_client()
            if not self.winlirc.connected :
                del self.winlirc
                self.winlirc=None
                self.lirc_state.SetLabel('disconnected')
            else:
                self.lirc_state.SetLabel('connected')
                self.timer1.Start(200)
        elif self.winlirc != None and self.winlirc.thrd != None:
            self.timer1.Stop()
            self.winlirc.close()
            self.winlirc.thrd.join(0.2)
            self.lirc_state.SetLabel('disconnected')
            del self.winlirc
            self.winlirc=None
        if event != None:
            event.Skip()

    def OnSettingsdlgClose(self, event):
        self.timer1.Stop()
        if self.winlirc != None and self.winlirc.thrd != None:
            self.winlirc.close()
            self.winlirc.thrd.join(0.2)
            del self.winlirc
            self.winlirc=None
        event.Skip()

    def OnTimer1Timer(self, event):
        if self.winlirc != None:
            if self.winlirc.connected:
                cmd=self.winlirc.getButtonName()
                if len(cmd) > 0:
                    self.listCtrl1.SetStringItem(self.listCtrl1.GetFirstSelected(),1,cmd)
            else:
                self.lirc_state.SetLabel('disconnected')
        event.Skip()

    def OnListCtrl1ListItemDeselected(self, event):
        self.currentItem = event.m_itemIndex
        listitem=self.listCtrl1.GetItem(self.currentItem,1)
        text=listitem.GetText()
        if text=='.......':
            self.listCtrl1.SetStringItem(self.currentItem,1,'<db click & press IR>')
        event.Skip()

    def OnUse_cacheCheckbox(self, event):
        self.cachesize.Enable(self.use_cache.IsChecked())
        event.Skip()

    def OnFontBrowse(self, event):
        dlg = wx.FileDialog(self, "Choose a font descriptor file", "mplayer\\font\\", "", "*.desc", wx.OPEN)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                self.fontpath.SetValue(dlg.GetPath())
        finally:
            dlg.Destroy()
        event.Skip()

    def OnDefvolumeScroll(self, event):
        self.dv_view.SetValue(str(self.defvolume.GetValue()))
        event.Skip()
