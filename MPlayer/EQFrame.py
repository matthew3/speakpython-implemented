#Boa:Frame:EQFrame

import wx

def create(parent):
    return EQFrame(parent)

[wxID_EQFRAME, wxID_EQFRAMEPANEL1, wxID_EQFRAMESTATICBITMAP1, 
 wxID_EQFRAMESTATICBITMAP10, wxID_EQFRAMESTATICBITMAP11, 
 wxID_EQFRAMESTATICBITMAP12, wxID_EQFRAMESTATICBITMAP2, 
 wxID_EQFRAMESTATICBITMAP3, wxID_EQFRAMESTATICBITMAP4, 
 wxID_EQFRAMESTATICBITMAP5, wxID_EQFRAMESTATICBITMAP6, 
 wxID_EQFRAMESTATICBITMAP7, wxID_EQFRAMESTATICBITMAP8, 
 wxID_EQFRAMESTATICBITMAP9, 
] = [wx.NewId() for _init_ctrls in range(14)]

class EQFrame(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_EQFRAME, name='EQFrame', parent=prnt,
              pos=wx.Point(544, 432), size=wx.Size(125, 170),
              style=wx.SIMPLE_BORDER, title='Eq')
        self.SetClientSize(wx.Size(117, 143))

        self.panel1 = wx.Panel(id=wxID_EQFRAMEPANEL1, name='panel1',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(117, 143),
              style=wx.TAB_TRAVERSAL)
        self.panel1.SetBackgroundColour(wx.Colour(212, 208, 200))
        self.panel1.SetBackgroundStyle(wx.BG_STYLE_SYSTEM)

        self.staticBitmap1 = wx.StaticBitmap(bitmap=wx.Bitmap('images/vol.png',
              wx.BITMAP_TYPE_PNG), id=wxID_EQFRAMESTATICBITMAP1,
              name='staticBitmap1', parent=self.panel1, pos=wx.Point(56, 75),
              size=wx.Size(10, 30), style=0)

        self.staticBitmap2 = wx.StaticBitmap(bitmap=wx.Bitmap('images/volup.png',
              wx.BITMAP_TYPE_PNG), id=wxID_EQFRAMESTATICBITMAP2,
              name='staticBitmap2', parent=self.panel1, pos=wx.Point(48, 48),
              size=wx.Size(20, 10), style=0)
        self.staticBitmap2.Bind(wx.EVT_LEFT_DOWN, self.OnStaticBitmap2LeftDown)

        self.staticBitmap3 = wx.StaticBitmap(bitmap=wx.Bitmap('images/voldown.png',
              wx.BITMAP_TYPE_PNG), id=wxID_EQFRAMESTATICBITMAP3,
              name='staticBitmap3', parent=self.panel1, pos=wx.Point(48, 120),
              size=wx.Size(20, 10), style=0)
        self.staticBitmap3.Bind(wx.EVT_LEFT_DOWN, self.OnStaticBitmap3LeftDown)

        self.staticBitmap4 = wx.StaticBitmap(bitmap=wx.Bitmap('images/vol.png',
              wx.BITMAP_TYPE_PNG), id=wxID_EQFRAMESTATICBITMAP4,
              name='staticBitmap4', parent=self.panel1, pos=wx.Point(88, 75),
              size=wx.Size(10, 30), style=0)

        self.staticBitmap5 = wx.StaticBitmap(bitmap=wx.Bitmap('images/volup.png',
              wx.BITMAP_TYPE_PNG), id=wxID_EQFRAMESTATICBITMAP5,
              name='staticBitmap5', parent=self.panel1, pos=wx.Point(80, 48),
              size=wx.Size(20, 10), style=0)
        self.staticBitmap5.Bind(wx.EVT_LEFT_DOWN, self.OnStaticBitmap5LeftDown)

        self.staticBitmap6 = wx.StaticBitmap(bitmap=wx.Bitmap('images/voldown.png',
              wx.BITMAP_TYPE_PNG), id=wxID_EQFRAMESTATICBITMAP6,
              name='staticBitmap6', parent=self.panel1, pos=wx.Point(80, 120),
              size=wx.Size(20, 10), style=0)
        self.staticBitmap6.Bind(wx.EVT_LEFT_DOWN, self.OnStaticBitmap6LeftDown)

        self.staticBitmap7 = wx.StaticBitmap(bitmap=wx.Bitmap('images/vol.png',
              wx.BITMAP_TYPE_PNG), id=wxID_EQFRAMESTATICBITMAP7,
              name='staticBitmap7', parent=self.panel1, pos=wx.Point(24, 75),
              size=wx.Size(10, 30), style=0)
        self.staticBitmap7.SetBackgroundStyle(wx.BG_STYLE_COLOUR)

        self.staticBitmap8 = wx.StaticBitmap(bitmap=wx.Bitmap('images/volup.png',
              wx.BITMAP_TYPE_PNG), id=wxID_EQFRAMESTATICBITMAP8,
              name='staticBitmap8', parent=self.panel1, pos=wx.Point(16, 48),
              size=wx.Size(20, 10), style=0)
        self.staticBitmap8.Bind(wx.EVT_LEFT_DOWN, self.OnStaticBitmap8LeftDown)

        self.staticBitmap9 = wx.StaticBitmap(bitmap=wx.Bitmap('images/voldown.png',
              wx.BITMAP_TYPE_PNG), id=wxID_EQFRAMESTATICBITMAP9,
              name='staticBitmap9', parent=self.panel1, pos=wx.Point(16, 120),
              size=wx.Size(20, 10), style=0)
        self.staticBitmap9.Bind(wx.EVT_LEFT_DOWN, self.OnStaticBitmap9LeftDown)

        self.staticBitmap10 = wx.StaticBitmap(bitmap=wx.Bitmap('images/brightness.png',
              wx.BITMAP_TYPE_PNG), id=wxID_EQFRAMESTATICBITMAP10,
              name='staticBitmap10', parent=self.panel1, pos=wx.Point(16, 16),
              size=wx.Size(16, 16), style=0)

        self.staticBitmap11 = wx.StaticBitmap(bitmap=wx.Bitmap('images/contrast.png',
              wx.BITMAP_TYPE_PNG), id=wxID_EQFRAMESTATICBITMAP11,
              name='staticBitmap11', parent=self.panel1, pos=wx.Point(48, 16),
              size=wx.Size(16, 16), style=0)

        self.staticBitmap12 = wx.StaticBitmap(bitmap=wx.Bitmap('images/hue.png',
              wx.BITMAP_TYPE_PNG), id=wxID_EQFRAMESTATICBITMAP12,
              name='staticBitmap12', parent=self.panel1, pos=wx.Point(80, 16),
              size=wx.Size(16, 16), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)

    def OnStaticBitmap8LeftDown(self, event):
        if hasattr(self, '__ostream__'):
            self.__ostream__.write("brightness +5\n")
        else:
            prnt=self.GetParent()
            self.__ostream__=prnt.ostream
        event.Skip()

    def OnStaticBitmap9LeftDown(self, event):
        if hasattr(self, '__ostream__'):
            self.__ostream__.write("brightness -5\n")
        else:
            prnt=self.GetParent()
            self.__ostream__=prnt.ostream
        event.Skip()

    def OnStaticBitmap2LeftDown(self, event):
        if hasattr(self, '__ostream__'):
            self.__ostream__.write("contrast +5\n")
        else:
            prnt=self.GetParent()
            self.__ostream__=prnt.ostream
        event.Skip()

    def OnStaticBitmap3LeftDown(self, event):
        if hasattr(self, '__ostream__'):
            self.__ostream__.write("contrast -5\n")
        else:
            prnt=self.GetParent()
            self.__ostream__=prnt.ostream
        event.Skip()

    def OnStaticBitmap5LeftDown(self, event):
        if hasattr(self, '__ostream__'):
            self.__ostream__.write("hue +5\n")
        else:
            prnt=self.GetParent()
            self.__ostream__=prnt.ostream
        event.Skip()

    def OnStaticBitmap6LeftDown(self, event):
        if hasattr(self, '__ostream__'):
            self.__ostream__.write("hue -5\n")
        else:
            prnt=self.GetParent()
            self.__ostream__=prnt.ostream
        event.Skip()
