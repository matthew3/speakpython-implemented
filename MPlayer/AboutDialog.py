#Boa:Dialog:Aboutdlg

##///////////////////////////////////////////////////////////////////////////
## Name:        AboutDialog.py
## Purpose:     About dialog of MPlayer Control
## Author:      Tamas Zelena
## Modified by:
## Created:     09/07/2004
## Copyright:   (c) Tamas Zelena
## Licence:     GNU-GPL
##///////////////////////////////////////////////////////////////////////////

import wx

def create(parent):
    return Aboutdlg(parent)

[wxID_ABOUTDLG, wxID_ABOUTDLGSTATICBITMAP1, wxID_ABOUTDLGSTATICTEXT1, 
 wxID_ABOUTDLGSTATICTEXT11, wxID_ABOUTDLGSTATICTEXT12, 
 wxID_ABOUTDLGSTATICTEXT13, wxID_ABOUTDLGSTATICTEXT14, 
 wxID_ABOUTDLGSTATICTEXT2, wxID_ABOUTDLGSTATICTEXT3, wxID_ABOUTDLGSTATICTEXT4, 
] = [wx.NewId() for _init_ctrls in range(10)]

class Aboutdlg(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_ABOUTDLG, name='Aboutdlg', parent=prnt,
              pos=wx.Point(451, 361), size=wx.Size(347, 426),
              style=wx.THICK_FRAME | wx.STAY_ON_TOP | wx.DEFAULT_DIALOG_STYLE,
              title='About')
        self.SetClientSize(wx.Size(339, 399))
        self.SetAutoLayout(False)
        self.Bind(wx.EVT_LEFT_DOWN, self.OnAboutdlgLeftDown)

        self.staticText11 = wx.StaticText(id=wxID_ABOUTDLGSTATICTEXT11,
              label=u'v 1.0.1', name='staticText11', parent=self,
              pos=wx.Point(112, 115), size=wx.Size(112, 16),
              style=wx.ST_NO_AUTORESIZE | wx.ALIGN_CENTRE)
        self.staticText11.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD,
              False, u'Microsoft Sans Serif'))

        self.staticText12 = wx.StaticText(id=wxID_ABOUTDLGSTATICTEXT12,
              label='NO WARRANTY', name='staticText12', parent=self,
              pos=wx.Point(120, 200), size=wx.Size(93, 13), style=0)
        self.staticText12.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.BOLD,
              False, 'MS Shell Dlg'))

        self.staticText13 = wx.StaticText(id=wxID_ABOUTDLGSTATICTEXT13,
              label=u'Copyright by Tamas Zelena @ 2004-2006',
              name='staticText13', parent=self, pos=wx.Point(72, 152),
              size=wx.Size(199, 13), style=0)

        self.staticText14 = wx.StaticText(id=wxID_ABOUTDLGSTATICTEXT14,
              label='(e-mail : ze.tor@freemail.hu)', name='staticText14',
              parent=self, pos=wx.Point(104, 168), size=wx.Size(144, 13),
              style=0)

        self.staticText1 = wx.StaticText(id=wxID_ABOUTDLGSTATICTEXT1,
              label='MPlayer Control is a frontend for MPlayer.\nMPlayer is the best/fast media player under GNU-GPL.\nSee MPlayer license and other info in mplayer subdirectory.\n\nMPlayer is (C) 2000 2005\n\t\tThe MPlayer Team',
              name='staticText1', parent=self, pos=wx.Point(40, 232),
              size=wx.Size(272, 96), style=wx.ST_NO_AUTORESIZE)

        self.staticText2 = wx.StaticText(id=wxID_ABOUTDLGSTATICTEXT2,
              label='License: GNU-GPL', name='staticText2', parent=self,
              pos=wx.Point(112, 136), size=wx.Size(107, 13), style=0)
        self.staticText2.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.BOLD, False,
              'MS Shell Dlg'))

        self.staticText3 = wx.StaticText(id=wxID_ABOUTDLGSTATICTEXT3,
              label=u'MPlayer source is found on:  http://www.mplayerhq.hu',
              name='staticText3', parent=self, pos=wx.Point(40, 328),
              size=wx.Size(265, 13), style=0)

        self.staticText4 = wx.StaticText(id=wxID_ABOUTDLGSTATICTEXT4,
              label=u'Used version: MPlayer 1.0-pre8', name='staticText4',
              parent=self, pos=wx.Point(40, 352), size=wx.Size(152, 13),
              style=0)

        self.staticBitmap1 = wx.StaticBitmap(bitmap=wx.Bitmap('images/logo.png',
              wx.BITMAP_TYPE_PNG), id=wxID_ABOUTDLGSTATICBITMAP1,
              name='staticBitmap1', parent=self, pos=wx.Point(20, 8),
              size=wx.Size(300, 100), style=0)
        self.staticBitmap1.Bind(wx.EVT_LEFT_DOWN, self.OnStaticBitmap1LeftDown)

    def __init__(self, parent):
        self._init_ctrls(parent)
        x,y=self.GetSizeTuple()
        self.SetPosition((wx.SystemSettings_GetMetric(wx.SYS_SCREEN_X)/2-x/2,wx.SystemSettings_GetMetric(wx.SYS_SCREEN_Y)/2-y/2))

    def OnAboutdlgLeftDown(self, event):
        self.Close()
        event.Skip()

    def OnStaticBitmap1LeftDown(self, event):
        self.Close()
        event.Skip()
