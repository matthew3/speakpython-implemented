#!/usr/bin/env python
#Boa:App:BoaApp

##///////////////////////////////////////////////////////////////////////////
## Name:        pyMplayerC.py
## Purpose:     wxApp of MPlayer Control
## Author:      Tamas Zelena
## Modified by:
## Created:     09/07/2004
## Copyright:   (c) Tamas Zelena
## Licence:     GNU-GPL
##///////////////////////////////////////////////////////////////////////////
#import wxversion
#wxversion.select('2.6')
import wx
import Main,os
import traceback
import win32gui,win32con
import struct,array
import sys

modules ={'AboutDialog': [0, '', 'AboutDialog.py'],
 'DVD_dialog': [0, '', 'DVD_dialog.py'],
 'EQFrame': [0, '', 'EQFrame.py'],
 'FilesDialog': [0, '', 'FilesDialog.py'],
 'Main': [1, 'Main frame of Application', 'Main.py'],
 'PListFrame': [0, '', 'PListFrame.py'],
 'Settings': [0, '', 'Settings.py'],
 'playlist': [0, '', 'playlist.py'],
 'winlirc': [0, '', 'winlirc.py']}

class BoaApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        prgdir=os.path.dirname(sys.argv[0])
        if prgdir <> '':
            os.chdir(prgdir)
        self.main = Main.create(None)
        # needed when running from Boa under Windows 9X
        self.main.Show()
        self.SetTopWindow(self.main)
        if len(sys.argv) > 1:
            self.main.fname='"'+sys.argv[1]+'"'
            self.main.filename.SetLabel(self.main.fname[:44])
            self.main.fnamepos=0
            self.main.irany=1
            self.main.plwin.listCtrl1.DeleteAllItems()
            cnt=self.main.plwin.listCtrl1.GetItemCount()
            self.main.play_type=2
            r,e=os.path.splitext(self.main.fname)
            if e.lower()=='.pls"' or e.lower()=='.m3u"':
                self.main.ParseM3U(self.main.fname)
            else:
                self.main.plwin.listCtrl1.InsertStringItem(cnt,' ')
                self.main.plwin.listCtrl1.SetStringItem(cnt,1,self.main.fname.strip('"'))
                self.main.plwin.listCtrl1.SetStringItem(cnt,2,self.main.subname.strip('"'))

            self.main.OnPlay(None)
        return True

def main():
    try:
        hwnd=win32gui.FindWindow(None,'MPlayer Control')
        if len(sys.argv) > 1 and hwnd != 0:
##            buf=win32gui.PyMakeBuffer(len(sys.argv[1])+2)
##            a,l=win32gui.PyGetBufferAddressAndLen(buf)
##            fn='"'+sys.argv[1]+'"'
##            print fn
##            win32gui.PySetString(a,'"'+fn+'"')
##            win32gui.SendMessage(hwnd,win32con.WM_COPYDATA,l,a)
##            int_buffer = array.array("L", [0])
##            char_buffer = array.array("c", "the string data")
##            int_buffer_address = int_buffer.buffer_info()[0]
##            char_buffer_address, char_buffer_size = char_buffer.buffer_info
##            copy_struct = struct.pack("pLp",  # dword *, dword, char *
##                           int_buffer_address,
##                           char_buffer_size, char_buffer_address)
##            # find target_hwnd somehow.
##            win32gui.SendMessage(hwnd, win32con.WM_COPYDATA, hwnd, copy_struct)
            cds=packMsg('"'+sys.argv[1]+'"')
            win32gui.SendMessage(hwnd, win32con.WM_COPYDATA, 0 ,
                cds[0])
            return
    except Exception, e:
        print e
        pass
##        excep=sys.exc_info()
##        print traceback.format_exception_only(excep[0],excep[1])[0]
    application = BoaApp(0)
    application.MainLoop()

def packMsg(msg):
    # Don't want ar to go away while being sent to another window
    global ar,cds1,cds2,cds
    ar=array.array('c',msg)
    msg=ar.buffer_info()
    cds1 = struct.pack('LLP',0, msg[1],msg[0])
    cds2 = array.array('c', cds1)
    cds = cds2.buffer_info()
    return cds

if __name__ == '__main__':
    main()
