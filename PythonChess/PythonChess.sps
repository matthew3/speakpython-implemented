#num() = ([one] | [two] | [three] | [four] | [five] | [six] | [seven] | [eight]);
@results
	0 {'7'}
	1 {'6'}
	2 {'5'}
	3 {'4'}
	4 {'3'}
	5 {'2'}
	6 {'1'}
	7 {'0'}
@

#letter() = ([a|ey|alpha] | [b|bravo|beta] | [c|charlie] | [d|delta] | [e|echo|epsilon] | [f|fox|foxtrot] | [g|golf|gamma] | [h|hotel|hospital]);
@results
	0 {'0'}
	1 {'1'}
	2 {'2'}
	3 {'3'}
	4 {'4'}
	5 {'5'}
	6 {'6'}
	7 {'7'}
@

#position() = #letter #num;
@results
	#num,#letter {'(' #num ',' #letter ')'}
@

((move|put|bring|place) (the piece (at|from)?))? #position (on|to) #position;
@results
	#position_0,#position_1 {'(' #position_0 ',' #position_1 ')'}
@

i want (the piece at)? #position to (go|(be brought)|(be placed)|(to|on)) #position;
@results
	#position_0,#position_1 {'(' #position_0 ',' #position_1 ')'}
@

from #position (move|put|bring|place) (the piece)? to #position;
@results
	#position_0,#position_1 {'(' #position_0 ',' #position_1 ')'}
@

#position to #position;
@results
	#position_0,#position_1 {'(' #position_0 ',' #position_1 ')'}
@

#piece() = ([pawn] | [rook|tower] | [knight|horse|horsy] | [bishop] | [queen] | [king]);
@results
	0 {'P'}
	1 {'R'}
	2 {'T'}
	3 {'B'}
	4 {'Q'}
	5 {'K'}
@

((move|put|place|bring) (my)?)? #piece (to|on) #position;
@results
	#piece,#position {#piece #position}
@
